# Part III: Fulfilling The Blue Angel Award Criteria

![Monitoring energy and hardware consumption in real time with KDE's `LabPlot`. (Image from Alexander Semke published under a [CC-BY-NC-ND-4.0](https://creativecommons.org/licenses/by-nc-nd/4.0/) license.)](images/sec3_labplot_live-measurement.png)

The three main categories of the Blue Angel award criteria for desktop software are:

 - (A) Resource & Energy Efficiency

 - (B) Potential Hardware Operating Life

 - (C) User Autonomy

In this section we'll provide a hands-on guide to fulfilling each set of criteria. There are numerous benefits of meeting the basic award criteria. By making the energy consumption of your software transparent and complying with the hardware operating life and user autonomy criteria, you get the benefits of:

 - **Eco-Certification**: Apply for the Blue Angel ecolabel to demonstrate to users, companies, and governmental organizations that your software is designed sustainably.

 - **Data-Driven Development**: Locate inefficiencies in terms of energy and hardware consumption, and make data-driven decisions for your software development.

 - **Sustainable Design**: For the long-term sustainable use of software, and thus hardware, take the user autonomy criteria into consideration when planning your software design.
 
 - **End-User Information**: Highlight to your users the ways your software is already sustainably designed by using the Blue Angel criteria as a benchmark.

![The three steps to eco-certification: 1. Measure, 2. Analyze, 3. Certify. (Image from KDE published under a [CC BY-SA 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/deed.en) license. [Certificate](https://thenounproject.com/icon/certificate-5461357/) icon by Ongycon licensed under a [CC-BY](https://spdx.org/licenses/CC-BY-3.0.html) license. Design by Lana Lutz.)](images/sec3_3StepsToBEECO.png)

## (A) How To Measure Your Software

The laboratory setup consists of a power meter, a computer to aggregate and evaluate the power meter output, and a desktop computer for the system under test where user behavior is emulated. The setup described here follows the specifications from the ["*Blue Angel Basic Award Criteria for Resource and Energy-Efficient Software Products*"](https://www.blauer-engel.de/en/productworld/resources-and-energy-efficient-software-products).

Terminology comes in part from Kern et al. (2018): ["*Sustainable software products &mdash; Towards assessment criteria for resource and energy efficiency*"](https://doi.org/10.1016/j.future.2018.02.044).

See also the following resources from the Umwelt Campus Birkenfeld:

 - Seiwert & Zaczyk (2021): ["*Projektbericht: Ressourceneffiziente Softwaresysteme am Beispiel von KDE-Software*"](https://invent.kde.org/cschumac/feep/-/blob/master/measurements/abschlussbericht-kmail-krita.pdf) (German only)

 - Mai (2021): "*Vergleichende Analyse und Bewertung von Betriebssystemen hinsichtlich ihrer Energieeffizienz*" (German only)

 - ["*OSCAR Manual*"](https://gitlab.umwelt-campus.de/y.becker/oscar-public/blob/master/OSCAR/static/OSCAR_Gesamt.pdf) (German only)

### Overview Of Laboratory Setup

The laboratory setup requires 1 power meter and at least 2 computers:

 - *Power Meter*

    One of the devices recommended by the Blue Angel is the [Gude Expert Power Control 1202 Series](https://www.gude.info/en/power-distribution/switched-metered-pdu/expert-power-control-1202-series.html) ([manual](https://gude-systems.com/app/uploads/2022/05/manual-epc1202-series.pdf)). It provides outlets for powering the computer and measures the current during operation. The device can be controlled and read via cabled Ethernet. There is a web-based user interface, a [REST API](http://wiki.gude.info/EPC_HTTP_Interface), and the device supports various protocols such as SNMP or syslog.

 - *Computer 1: Data Aggregator & Evaluator*

    The computer will be used for collecting and evaluating results from the power meter.

    A Python script to read out the data from the Gude Expert Power Control 1202 Series is available at the [FEEP repository](https://invent.kde.org/teams/eco/feep/-/tree/master/tools/GUDEPowerMeter).
    
    It is recommended to monitor progress live with the second computer in order to ensure everything is proceeding smoothly. This can be done with KDE's [Labplot](https://apps.kde.org/labplot2/), for instance; read more [here](#sec:labplot).

    Other power meters may require non-Free software, e.g., Janitza's [GridVis Power Grid Monitoring Software](https://www.gridvis.com/gridvis-overview.html).

![Screenshot of the Gude Power Meter.](images/sec3_gude-pm.jpg)

  - *Computer 2: System Under Test*

    The reference system is the hardware used to measure the energy consumption of the "system under test", or SUT. The SUT includes the operating system and software installed for (i) testing the software product, (ii) emulating the standard usage scenario[^2] and (iii) collecting the hardware performance results.

[^2]: It is possible to have a setup using 3 computers, with the Standard Usage Scenario emulation generated on a computer independent of the SUT; see Kern et al. (2018). Details of such a setup with an external workload generator can be [found at the FEEP repository](https://invent.kde.org/teams/eco/feep/-/tree/master/tools/arduino-board_external-workload-generator.md).
 
Note the following:

 - For GNU/Linux systems, the [Blue Angel criteria (Section 1.1)](https://www.blauer-engel.de/en/productworld/resources-and-energy-efficient-software-products) require one of several Fujitsu computers as the reference system.

 - For emulating activity in the standard usage scenario, Free Software task automation tools such as [`xdotool`](https://github.com/jordansissel/xdotool), [`KDE Eco Tester` (in progress)](https://invent.kde.org/teams/eco/feep/-/tree/master/tools/KdeEcoTest), or [`Actiona`](https://wiki.actiona.tools/doku.php?id=en:start) (GPLv3) can be used.

 - For collecting hardware performance data (e.g., processor and RAM utilization, hard disk activity, network traffic), the Free Software tool [`Collectl`](https://sourceforge.net/projects/collectl/) (GPLv2/Artistic License) is available.

 - It's also possible to [repurpose cheap switchable power plugs as measurement devices](https://volkerkrause.eu/2020/10/17/kde-cheap-power-measurement-tools.html); see Section ["Alternative: Gosund SP111 Setup"](#sec:gosundPM) for set up instructions.

![Overview of laboratory setup: System Under Test (SUT), Power Meter (PM), and Data Aggregator and Evaluator (DAE). (Image from KDE published under a [CC-BY-SA-4.0](https://spdx.org/licenses/CC-BY-SA-4.0.html) license. Design by Lana Lutz.)](images/sec3_lab-setup_modified.png)

#### System Under Test (SUT)

For instance, the Fujitsu Esprimo P920 Desktop-PC proGreen selection (Intel Core i5-4570 3,6GHz, 4GB RAM, 500GB HDD) is one of the recommended reference systems; see Appendix D in the award criteria for other recommended Fujitsu systems.

On the reference system you need to set up the SUT, i.e., the system on which you will test the software. The SUT must reduce unrelated energy consumption and have a standardized configuration. Recommended is the following:

 - Overwrite the entire hard drive of the machine with a standardized OS.

 - Deactivate all possible background processes (automatic updates, backups, indexing, etc.).

 - Install the necessary software, i.e., the application to be measured as well as the user emulation (e.g., `xdotool`) and hardware performance data (e.g., `Collectl`) software.
 
 - When running the usage scenario scripts, the cache should be cleared between runs and any new files deleted before starting the next measurement.

#### Preparing The Standard Usage Scenario (SUS)

Preparing the SUS requires the following:

 - Identifying tasks users typically carry out when using the application under consideration.

 - Identifying functionalities which require high energy demand or high resource utilization.

 - Based on the above, scheduling a flow chart of individual actions and emulating these actions with a task automation tool.
 
 - Scheduling a wait-period of 60 seconds before starting the measurement is recommended.

 - Running the SUS for at least 5 minutes.

![Steps for preparing Standard Usage Scenario (SUS) scripts to measure the energy consumption of software. (Image from KDE published under a [CC-BY-SA-4.0](https://spdx.org/licenses/CC-BY-SA-4.0.html) license. Design by Lana Lutz.)](images/sec3_PreparingSUS.png)

An automation tool is needed to run the usage scenarios so as not to require human intervention. In this way the script can be run repeatedly in a well-defined manner to provide accurate measurements.

Example tasks and functions tested in the SUS for KDE's email client [`KMail`](https://apps.kde.org/kmail2/) include searching for an email, writing a reply or forwarding the email, saving an attachment, deleting a folder in the mail client, etc. See the [Actiona scripts](https://invent.kde.org/teams/eco/feep/-/tree/master/measurements) used to test Krita and Okular for further examples.

Important: If the emulation tool uses pixel coordinates to store the position of the automated clicks (e.g., `Actiona`) and, moreover, the screen resolution of the computer used in preparation differs from that of the laboratory computer, all pixel coordinates will have to be reset for the laboratory environment.

##### More Emulation Tools

Beyond [`xdotool`](https://github.com/jordansissel/xdotool), [`KDE Eco Tester` (in progress)](https://invent.kde.org/teams/eco/feep/-/tree/master/tools/KdeEcoTest), or [`Actiona`](https://github.com/Jmgr/actiona), there are other candidates for tools which might meet the requirements. See a list from KDE Contributor David Hurka in the presentation ["*Visual Workflow Automation Tools*"](https://invent.kde.org/teams/eco/feep/-/tree/master/tools/presentation_Visual_Workflow_Automation_Tools). Most of the tools use X11-specific features, and thus do not work on Wayland systems. There are a few possible approaches here:

 - [Selenium Webdriver using AT-SPI](https://invent.kde.org/sdk/selenium-webdriver-at-spi) (currently in testing for [Season of KDE 2023](https://dot.kde.org/2023/01/23/season-kde-2023-mentees-and-projects))

 - [The XDG RemoteDesktop portal](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-org.freedesktop.portal.RemoteDesktop)

 - Various Wayland protocols (support varies between compositors):
    - <https://github.com/swaywm/wlr-protocols/blob/master/unstable/wlr-virtual-pointer-unstable-v1.xml>
    - <https://api.kde.org/frameworks/kwayland/html/classKWayland_1_1Client_1_1FakeInput.html>

 - [libinput user devices](https://lwn.net/Articles/801767/)

### Measurement Process

The measurement process is defined in Appendix A of the [Basic Award Criteria](https://www.blauer-engel.de/en/productworld/resources-and-energy-efficient-software-products). It requires recording and logging energy data and performance indicators with a granularity of 1-second so that they can be processed and average values can be calculated.

Some general comments:

 - Times between the PM and DAE must be synchronized.

 - When using `Collectl` to collect performance load, ensure it is running in the console of the SUT; also, check that the required CSV file is correctly generated before testing.

 - Since each run of the usage scenarios results in changes to the standard operating system, clearing the cache between runs is recommended.

 - All runs (Baseline, Idle Mode, Standard Usage Scenario) must be for the same length of time, based on the time needed to run the usage scenario script.

 - On the DAE you may want to confirm that the desired power outlet is read out correctly before and/or during testing (e.g., with a live graph using `LabPlot`).


During the energy measurements, [`Collectl`](https://sourceforge.net/projects/collectl/) is used to record a set of performance indicators: processor utilisation, RAM utilisation, hard disk activity, and network traffic. Use the following command to obtain this hardware performance data:

`$ collectl -s cdmn -i1 -P --sep 59 -f ~/<FILENAME>.csv`

The options are as follows:

 - `-s cdmn`
 
    collect CPU, Disk, memory, and network data

 - `-i1`

    sampling interval of 1 second

 - `-P`

    output in plot format (separated data which consists of a header with one line per sampling interval)

 - `--sep 59`

    semicolon separator

 - `-f </PATH/TO/FILE>.csv`

    save file at specified path

#### Measuring Baseline, Idle Mode, And Standard Usage Scenarios

 -  Baseline Scenario: *Operating System* (OS)

    To establish the baseline, a scenario is measured in which the OS is running but no actions are taken.

 -  Idle Mode Scenario: *OS + Application While Idle*

    To establish the energy consumption and hardware performance data of the application while idle, a scenario is measured in which the application under consideration is opened but no action is taken.

    Important: the baseline and idle mode must be run for the same time needed to carry out the standard usage scenario. Since the power consumption for the baseline and idle scenario is relatively uniform, 10 repetitions for each is considered sufficient to obtain a representative sample (Seiwert & Zaczyk 2021).

 -  Standard Usage Scenario: *OS + Application In Use*

    To measure the energy consumption and hardware performance data of the application in use, the standard usage scenario should be run; see SUS preparation notes above. The measurement of the standard usage scenario should be repeated 30 times, which will take several hours to complete. The higher number of repetitions is necessary to obtain a representative sample as the energy consumption and performance data may vary across measurements.

#### <a name="sec:labplot"></a> Monitoring Output With Labplot

You can use KDE's [LabPlot](https://apps.kde.org/labplot2/) to monitor the output live as data is coming in. To do this:

 - Redirect the power meter output to a CSV file.

 - In LabPlot, import the CSV file by selecting `File > Add New > Live Data Source ....`

 - Under "Filter", select the Custom option. Under "Data Format", define the separator value used (e.g., comma, semi-colon, space).

 - You can check that the output is correct under the "Preview" tab.
 
 - If everything looks good, click OK.
 
 - Finally, right-click on the data frame window and selecting `Plot Data > xy-Curve`.

### Analysis Of The Results With OSCAR

Once you have results, the Umwelt Campus Birkenfeld provides a useful tool for generating reports called `OSCAR` (**O**pen source **S**oftware **C**onsumption **A**nalysis in **R**):

  - [Source Code](https://gitlab.rlp.net/green-software-engineering/OSCAR-public)

  - [Running instance](https://oscar.umwelt-campus.de/)

See also the OSCAR Manual with detailed instructions, including additional screenshots on how to use `OSCAR`.

#### CSV Files 

Analysis with `OSCAR` requires uploading the following files to the [OSCAR website](https://oscar.umwelt-campus.de/):

 - (i) a log file of actions taken,
 - (ii) the energy consumption data, and
 - (iii) the hardware performance data. 

All files should be CSV files. Some preprocessing of the raw data may be necessary (e.g., performance data measured by `Collectl`; see below).

Important: `OSCAR` is **very** particular about data frame formats, including column names and cell values. The tables here provide examples which are confirmed to work. If you are having issues generating a report from your CSV files, make sure CSV files are as similar as possible to those shown here.

If you just want to test OSCAR, you can download data for Okular in [this ZIP file](https://invent.kde.org/teams/eco/feep/-/blob/master/measurements/okular/2022-09-22/2022-okular-data.zip). The data are confirmed to successfully generate a report using OSCAR v0.190404. The report which was generated can also be downloaded at the [FEEP repository](https://invent.kde.org/teams/eco/feep/-/blob/master/measurements/okular/2022-09-22/Bericht%20OSCAR%20Standardnutzungsszenario.pdf).

##### Log File Of Actions

The log file of actions should have the following format. Note the columns are separated by a semi-colon. Also, columns have no names (i.e., there is no header in the CSV file). Note that the start and end of each iteration must be labelled with 'startTestrun' and 'stopTestrun' in the second column, whereas the actions can be listed with any name.

|                       |                |         |
|-----------------------|----------------|---------|
| YYYY-MM-DD HH:MM:SS ; | startTestrun ; |         |
| YYYY-MM-DD HH:MM:SS ; |              ; | action1 |
| YYYY-MM-DD HH:MM:SS ; |              ; | action2 |
| YYYY-MM-DD HH:MM:SS ; |              ; | action3 |
| YYYY-MM-DD HH:MM:SS ; | stopTestrun  ; |         |

An example log file of actions for measuring KDE's text and code editor [`Kate`](https://apps.kde.org/kate/). The (i) date and time as well as (ii) start and stop times and (iii) actions are listed in three columns.

|                       |                |                               |
|-----------------------|----------------|-------------------------------|
| 2022-05-21 18:54:36 ; | startTestrun ; |                               |
| 2022-05-21 18:55:41 ; |              ; | go to line 100                |
| 2022-05-21 18:55:46 ; |              ; | toggle comment                |
| 2022-05-21 18:55:50 ; |              ; | find kconfig                  |
| 2022-05-21 18:55:55 ; |              ; | move between searches 6 times |
| 2022-05-21 18:56:05 ; |              ; | close find bar                |
| 2022-05-21 18:56:05 ; |              ; | standby 30 sec                |
| 2022-05-21 18:56:35 ; |              ; | go to line 200                |
| 2022-05-21 18:56:40 ; |              ; | select 10 lines               |
| 2022-05-21 18:56:43 ; |              ; | delete selected text          |
| [...]               ; |              ; | [...]                         |
| 2022-05-21 18:59:13 ; | stopTestrun  ; |                               |
 
##### Energy Consumption Data

The energy consumption data has the following format: the first column is the row number, the second column is the date and time in one-second increments, and the third column is the measurement output in Watts. Note that the following is confirmed to work with OSCAR: (i) the second and third column names as written below (i.e., "Zeit" and "Wert 1-avg[W]"), (ii) the date-time as a character string with the date and time separated by a comma, and (iii) no string delimiter used in the CSV file.

|   ; | Zeit ;               | Wert 1-avg[W] |
|-----|----------------------|---------------|
| 1 ; | DD.MM.YY, HH:MM:SS ; | value1        |
| 2 ; | DD.MM.YY, HH:MM:SS ; | value2        |
| 3 ; | DD.MM.YY, HH:MM:SS ; | value3        |
| 4 ; | DD.MM.YY, HH:MM:SS ; | value4        |

When using the Gude Power Meter with the [Python script](https://invent.kde.org/teams/eco/feep/-/tree/master/tools/GUDEPowerMeter) available at the FEEP repository, the timestamp will be recorded in [nanoseconds in Epoch time](https://en.wikipedia.org/wiki/Epoch_time). For instance, below is an example of the raw output for 7 rows from the Gude Power Meter output using the Python script. The first column shows the timestamp. The second column is the readout from the power meter in Watts.

|                    |    |
|--------------------|----|
| 1661611923019071 ; | 43 |
| 1661611923142924 ; | 43 |
| 1661611924293989 ; | 29 |
| 1661611924417017 ; | 28 |
| 1661611924744885 ; | 28 |
| 1661611924869051 ; | 28 |
| 1661611924992392 ; | 28 |

The raw data can be preprocessed in R: Nanoseconds in Epoch time can be converted to date-time with the command `as.POSIXct(<NANOSECONDS>/1000000, origin = '1970-01-01', tz = 'Europe/Berlin')`. For example, the nanoseconds in row 1 from the raw output is "2022-08-27 16:52:03 CEST" after conversion.

For use with OSCAR, this date-time should then be converted to a character string with the date as DD.MM.YY followed by a comma. All of this can be achieved with one command (this operation can be vectorized over the entire column in the data frame); replace the YYYY-MM-DD date with the date of your measurements: 

`stringr::str_replace(as.character(as.POSIXct(1661611923019071/1000000, origin = '1970-01-01', tz = 'Europe/Berlin')), '2022-08-27', '27.08.22,')`

The output in Watts should be averaged per second. The same data above is shown below after processing with R; note the 7 values above are averaged per second, resulting in two rows.

To save the CSV file with a semi-colon separator, the first column with row names starting at the number 1, and no string delimiter, use the following R command:

`write.csv2(<DATAFRAME>, file = <PATH/TO/FILE.csv>, row.names = TRUE, quote = FALSE)`

The result should look something like the following:

|  ;  | Zeit ;               | Wert 1-avg[W]   |
|-----|----------------------|-----------------|
| 1 ; | 27.08.22, 16:52:03 ; | 43.00000        |	
| 2 ; | 27.08.22, 16:52:04 ; | 28.20000        |

##### Performance Data (Raw)

When using `Collectl` for hardware performance data, it is necessary to do the following before uploading the data to OSCAR.[^3]

[^3]: See Seiwert & Zaczyk 2021: p. 13 for details; see also Appendix A 2 on p. 46 for a Python script to automate some of these tasks.

 - Remove all information above the header row.

 - Remove all \# characters from the file.

 - In the first column, no separator value should come between the data-time (otherwise, the date and time will be interpreted as two separate columns).

 - The date should also have a character inserted between YYYYMMDD, e.g., MM.DD.YYYY as above. Whatever character is used must be specified in `OSCAR`.
 
 - Column names can be anything you want as they will be specified within `OSCAR`.

 - The file must be saved in CSV format.

Moreover, the hardware performance output from `Collectl` includes many columns that are not necessary for analysis. The only measurements that need to be specified are the following columns:

 - [CPU]Totl = Processor
 - [MEM]Used = Main memory - used kilobytes
 - [NET]RxKBTot = Network - Kilobytes received/s
 - [NET]TxKBTot = Network - Kilobytes transmitted/s
 - [DSK]ReadKBTot = Disk - Kilobytes read/s
 - [DSK]WriteKBTot = Disk - kilobytes written/s.
 
Below is an example of the preprocessed results from `Collectl` measuring the performance data for Kate. The timestamp increases in one-second increments.

| Date-Time ; | cpu ; | mem ; | net_rec ; | net_trn ; | dsc_rd ; | dsc_wr |
|-|-|-|-|-|-|-|
| 27.08.2022 16:47:10 ; | 1 ; | 7131968 ; | 0 ; | 0 ; | 0 ; | 0   |
| 27.08.2022 16:47:11 ; | 4 ; | 7131968 ; | 0 ; | 0 ; | 0 ; | 0   |
| 27.08.2022 16:47:12 ; | 1 ; | 7131968 ; | 0 ; | 0 ; | 0 ; | 0   |
| 27.08.2022 16:47:13 ; | 1 ; | 7131968 ; | 0 ; | 0 ; | 0 ; | 120 |
| 27.08.2022 16:47:14 ; | 1 ; | 7131968 ; | 0 ; | 0 ; | 0 ; | 0   |
| 27.08.2022 16:47:15 ; | 1 ; | 7131968 ; | 0 ; | 0 ; | 0 ; | 56  |
| 27.08.2022 16:47:16 ; | 1 ; | 7131968 ; | 0 ; | 0 ; | 0 ; | 48  |
| 27.08.2022 16:47:17 ; | 1 ; | 7131968 ; | 0 ; | 0 ; | 0 ; | 0   |
| 27.08.2022 16:47:18 ; | 1 ; | 7131968 ; | 0 ; | 0 ; | 0 ; | 0   |
| 27.08.2022 16:47:19 ; | 4 ; | 7131968 ; | 0 ; | 0 ; | 0 ; | 132 |

#### Uploading Data

Once the above CSV files are ready, you can run the analysis using OSCAR, which will generate a summary report you can use either for eco-certification or for your own data-driven purposes. In the `OSCAR` interface, note the following:

 - The interface language is currently German only; see below for translations.

 - The duration of the measurements in seconds must be specified.

 - A semicolon separator is used.

 - The correct formatting of the time stamp must be specified for each of the uploaded files, e.g.,`%Y-%m-%d %H:%M:%OS`.

##### Step 1: Obtain Measurement Data

The landing page of the website (below) states that the first step is obtaining measurement data (German: *Erfassung Messdaten*). If you are at this point in the process, you should have already measured your software and prepared the CSV files.

![First obtain the measurement data (German: *Erfassung Messdaten*) in a laboratory.](images/sec3_oscar_1_landing.png)

All examples here are based on the Okular data in [this ZIP file](https://invent.kde.org/teams/eco/feep/-/blob/master/measurements/okular/2022-09-22/2022-okular-data.zip).

##### Step 2: Upload Measurement Data

Once you have the CSV files for the baseline, idle mode, and standard usage scenario measurements ready, click on *(2) Upload Messdaten* > *Upload*.

The measurement data (German: *Messdaten*) include the log file of actions (German: *Aktionen*), energy consumption (German: *Elektrische Leistung*), and hardware performance data (German: *Hardware-Auslastung*).

 - Under *Messungen*, upload either the idle mode or standard usage scenario measurement data.  

   For *Art der Messung* ('Type of Measurement') in the lower right of the UX, select *Leerlauf* ('Idle Mode') or *Nutzungsszenario* ('Usage Scenario') depending on which report you wish to generate.

 - Under *Baselines* upload the baseline measurement data.

 - Indicate the duration of the measurement scenarios in seconds (German: *Dauer der Einzelmessungen (s)*).

Note that the baseline measurements are always uploaded along with either the idle mode or standard usage scenario measurements.

See below for what a completed upload for the *Nutzungsszenario* looks like.

![Uploading the measurement data (German: *Upload Messdaten*).](images/sec3_oscar_2_upload.png)

***Timestamps*** &nbsp;&nbsp; Once the data has been uploaded, you will need to tell `OSCAR` how to read the data.

Let's start with the timestamp format (German: *Formatierung Zeitstempel*). This is one aspect of the process which can cause problems if not done correctly. This is done under *(2) Upload Messdaten* > *Formatierung Zeitstempel*.

Consider the Okular data:

 - For the log file of actions, the datetime is encoded as *YYYY-MM-DD HH:MM:SS* (e.g., "2022-10-04 12:32:43.656" in "okularActions.csv").
  
   In `OSCAR`, this is specified as "%Y-%m-%d %H:%M:%OS" (see screenshot below). OSCAR will take care of the fractional seconds.

 - For the energy consumption data, the datetime is encoded as *DD.MM.YY, HH:MM:SS* (e.g.,  "04.10.22, 12:32:43" in "okular_baseline_eletrLeistung.csv"). Note the period in the date and the comma seperating date from time, as well as only having two digits for the year.
  
   In `OSCAR` this is specified as "%d.%m.%y, %H:%M:%OS" (see screenshot below), in which the lowercase "%y" indicates a year with two digits.

 - For the hardware performance data, the datetime is encoded as *DD.MM.YYYY HH:MM:SS* (e.g.,  "04.10.2022 12:31:43" in "baseline_hardware_formatiert.csv"). Note the period in the date and the four-digit year.
  
   In `OSCAR`, this is specified as: "%d.%m.%Y %H:%M:%OS" (see screenshot below), in which the uppercase "%Y" indicates a year with four digits.

![Specifying the format of the timestamps (German: *Formatierung Zeitstempel*).](images/sec3_oscar_3_timestamp.png)

***Measurement Data*** &nbsp;&nbsp; After the timestamps have been correctly specified, let's explore the format of the measurement data (German: *Formatierung Messdaten*) in `OSCAR`.

First, take a look at the log file of actions (German: *Aktionen*). This is done under *(2) Upload Messdaten* > *Formatierung Messdaten* > *Aktionen*.

Here you need to indicate for the uploaded CSV file the separator (German: *Trennzeichen*), the string delimiter (German: *Textqualifizierer*), and the decimal separator (German: *Dezimaltrennzeichen*).

For the Okular data, this is defined as a semi-colon separator, double quotation string delimiter, and a period or full-stop decimal separator (see screenshot below).

Additionally, you will need to specify the following:

 - whether the first line contains headings (German: *Erste Zeile enthält Überschriften*);
 - the number of lines to skip (German: *Anzahl zu überspringender Zeilen*); and
 - the character encoding (German: *Zeichensatz (Encoding)*).
 
For the Okular data, this is defined as follows in the following screenshot: "first line contains headings" is unchecked, 0 lines are skipped, and character encoding is utf-8.

When everything is defined correctly, a preview of the spreadsheet will be shown.

![Specifying the format of the measurement data (German: *Formatierung Messdaten*) for the log file of actions (German: *Aktionen*).](images/sec3_oscar_4_actions.png)

Second, take a look at the energy consumption measurements (German: *Elektrische Leistung*). This is done under *(2) Upload Messdaten* > *Formatierung Messdaten* > *Elektrische Leistung*.

The required input is the same as for the log file of actions, seen in the following screenshot.

For the Okular data here, this is a semi-colon separator, double quotation string delimiter, and character encoding utf-8. However, now a comma is specified for the decimal separator and a checkmark indicates that the first line contains headings. Finally, the 1st line is skipped.

When everything is defined correctly, a preview of the spreadsheet will be shown.

![Specifying the format for the energy consumption data (German: *Elektrische Leistung*).](images/sec3_oscar_5_energy.png)

Finally, take a look at the hardware performance data (German: *Hardware-Auslastung*). This is done under *(2) Upload Messdaten* > *Formatierung Messdaten* > *Hardware-Auslastung*.

The required input is the same. The input in this example is a semi-colon separator, a double quotation string delimiter, a period (a.k.a. full stop) decimal separator. There is a checkmark that the first line contains headings, 0 lines are skipped, and the character encoding is utf-8.

However, now there is the additional requirement of specifying the columns (German: *Spalten*).

For the columns specification, the following need to be identified; select NA for unused columns, e.g., "Auslastung Auslagerungsdatei" here.

 - *Zeitstempel*: Datetime (i.e., 'Date-Time')
 - *CPU-Auslastung*: CPU (i.e., 'X.CPU.Totl')
 - *RAM-Auslastung*: RAM (i.e., 'X.MEM.Used')
 - *Über Netzwerk gesendet*: Network transmitted (i.e., 'X.NET.TxKBTot')
 - *Über Netzwerk empfangen*: Network received (i.e., 'X.NET.RxKBTot')
 - *Von Festplatte gelesen*: Disk read (i.e., 'X.DSK.ReadKBTot')
 - *Auf Festplatte geschrieben*: Disk written (i.e., 'X.DSK.WriteKBTot')
 - *Auslastung Auslagerungsdatei*: Swap (here, 'N/A')

![Specifying the format for the hardware performance data (German: *Hardware-Auslastung*).](images/sec3_oscar_6_hw.png)

##### Translations

Here is an overview of some of the German terminology used in `OSCAR` and the English translations:

 - *Messungen*: Measurements (e.g., Idle Mode or SUS)
 - *Aktionen*: Actions (i.e., log file of actions taken)
 - *Elektrische Leistung*: 'Electrical power' (i.e., energy consumption measurements)
 - *Hardware-Auslastung*: 'Hardware load' (i.e., hardware performance measurements)
 - *Dauer der Einzelmessungen (s)*: 'Duration of the individual measurements (s)' (i.e., specify how long each iteration was in seconds)
 - *Art der Messung*: 'Type of measurement'
     - *Leerlauf*: 'Idle' (i.e., Idle mode)
     - *Nutzungsszenario*: 'Usage scenario' (i.e., SUS)

 - *Formatierung Messdaten*: 'Formatting measurement data'
 - *Formatierung Zeitstempel*: 'Formatting timestamp'

 - *Trennzeichen* 'Separator'
 - *Textqualifizierer*: 'String delimiter'
 - *Dezimaltrennzeichen*: 'Decimal separator'
 - *Erste Zeile enthält Überschriften*: 'First line contains headings'
 - *Anzahl zu überspringender Zeilen*: 'Number of lines to skip'
 - *Zeichensatz (Encoding)*: 'Character set (encoding)'
 
 - *Spalten*: Columns
     - *Zeitstempel*: 'Datetime'
     - *CPU-Auslastung*: 'CPU utilization'
     - *RAM-Auslastung*: 'RAM utilization'
     - *Über Netzwerk gesendet*: 'Sent via network'
     - *Über Netzwerk empfangen*: 'Received via network'
     - *Von Festplatte gelesen*: 'Read from disk'
     - *Auf Festplatte geschrieben*: 'Written to disk'
     - *Auslastung Auslagerungsdatei*: 'Swap file utlization'

##### Step 3: Generating The Reports (Idle, SUS)

After completing the above, the report can be generated and downloaded. You will need to do this process twice, once for (i) the idle mode and (ii) standard usage scenario measurements, resulting in two documents.

![Generating the report (German: *Bericht erzeugen*).](images/sec3_oscar_7_report.png)

For Blue Angel eco-certification, the two reports will be submitted for evaluation by RAL.

For examples of the above for Okular, see KDE's [Blue Angel Applications](https://invent.kde.org/teams/eco/blue-angel-application) repository:

 - [OSCAR Report: Idle Mode](https://invent.kde.org/teams/eco/blue-angel-application/-/blob/master/applications/okular/de-uz-215-eng-annex-3-okular-idle.pdf)
 - [OSCAR Report: SUS](https://invent.kde.org/teams/eco/blue-angel-application/-/blob/master/applications/okular/de-uz-215-eng-annex-3-okular-scenario.pdf)

### Preparing The Documentation For Blue Angel

For Blue Angel eco-certification, it is necessary to complete several forms alongside the two reports from `OSCAR`.

Information that needs to be included in the forms is as follows:

 - Details about the software (name, version) and measurement process (when and where measurements were made, etc.).

 - Technical details about the power meter (instrument, sampling frequency, length of scenario, sample size).

 - Technical details about the reference system (year, model, processor, cores, etc.).

 - Software stack used for the measurements (`xdotool`, `Collectl`, etc.).

 - Minimum system requirements (processor architecture, local working memory, etc.).

 - Energy consumption results found in the `OSCAR` reports or equivalent.

 - Hardware utilization results, which includes the following (for IDLE use the idle mode measurements, and for SUS use the standard usage scenario measurements):
   - *Full Load*: "For processing power, the full load is 100%, for working memory the sum of the installed RAM capacities, for network bandwidth the maximum transmission speed, etc." (Blue Angel award criteria: p. 23).
   - *Base Load*: Average load for the reference system in Baseline measurements.
   - *Idle/SUS Load*: Average load for the reference system for IDLE/SUS measurements.
   
     From the above measurements, the following calculations are made for hardware utilization (for IDLE use the idle mode measurements, and for SUS use the standard usage scenario measurements):
     - *Net Load*: IDLE/SUS Load - Base Load
     - *Allocation Factor*: Net Load/(Full Load - Base Load)
     - *Effective Load*: Net Load + Allocation Factor * Base Load
     - *Hardware Utilization* (SUS only): Effective Load * Time (seconds)

For Blue Angel eco-certification, the above information will be added to two documents called "Annex 1" and "Annex 2".

For examples of the above for Okular, see KDE's [Blue Angel Applications](https://invent.kde.org/teams/eco/blue-angel-application) repository:

 - [Annex 1](https://invent.kde.org/teams/eco/blue-angel-application/-/blob/master/applications/okular/de-uz-215-eng-annex-1-okular.docx)
 - [Annex 2](https://invent.kde.org/teams/eco/blue-angel-application/-/blob/master/applications/okular/de-uz-215-eng-annex-2-okular.xlsx)

### <a name="sec:gosundPM"></a> Alternative: Gosund SP111 Setup

Want to get started with the process of measuring your software, but short on cash or gear? Want to give the process a try without setting up a dedicated lab? Try this hack converting an inexpensive power plug to a power meter, courtesy of Volker Krause, who also documented the process provided here. You can read more at the following posts from Volker's blog:

 - ["*Cheap Electric Power Measurement*"](https://web.archive.org/web/20221002125057/https://volkerkrause.eu/2020/10/17/kde-cheap-power-measurement-tools.html)

 - ["*KDE Eco Sprint July 2022*"](https://web.archive.org/web/20220819231538/https://www.volkerkrause.eu/2022/07/23/kde-eco-sprint-july-2022.html)

Below is a guide to setting up a [Gosund SP111](https://templates.blakadder.com/gosund_SP111_v2.html) power plug already [flashed](https://github.com/tasmota/docs/blob/development/docs/devices/BlitzWolf-SHP6.md) with [Tasmota firmware](https://tasmota.github.io/docs) in 10 steps.

Although the data from the inexpensive power meter will likely not be accepted by the Blue Angel for eco-certification, it is nonetheless possible to obtain preliminary data using this tool.

 - (0) *Prerequisite*
 
    It is necessary to have the power plug already flashed with a sufficiently new Tasmota version.

 - (1) *Firmware Reset*
 
    If the device had previously been connected to another Wifi network, it might need a full reset before being able to connect to a new one.

    If the device did open a WiFi access point named "tasmota-XXXXX" this is not needed, continue directly to (2).

    Press the button for 40 seconds.

    The device will restart and you should be able to continue at (2).

 - (2) *WiFi Setup*

    The device opens a WiFi access point named "tasmota-XXXXX"&mdash;connect to that.

    Open http://192.168.4.1 in a browser.

    The device will ask you for the WiFi name and password to connect to after entering those. The device will reconnect to that WiFi and disable its access point.

    While doing that it should show you its new address in the browser&mdash;make a note of it.

    In case that did not happen, check your WiFi router for the address of the device.

 - (3) *Tasmota Setup*

    Open the address from step (2) in a browser.

    You should see the Tasmota web UI (a big "ON/OFF" text and a bunch of blue and one red button).

    Click "Configuration".

    Click "Configure Other".

    Copy

            {"NAME":"Gosund SP111 2","GPIO":
            [56,0,57,0,132,134,0,0,131,17,0,21,0],"FLAG":0,
            "BASE":18}

    into the template input field.

    Tick the "Activate" checkbox.

    Click "Save".

    The device will restart; connect to it again.

    The UI should now also contain text fields showing electrical properties, and the "Toggle" button should now actually work.

 - (4) *Calibration*

    Open the address from step (2) in a browser.

    Connect a purely resistive load with a known wattage, such as a conventional light bulb (not a LED or energy-saving bulb).

    Switch on power by clicking "Toggle" if needed.

    Verify that the "Power Factor" value is shown as 1 (or very close to 1); if it is lower, the current load is not suited for calibration.

    Click "Console".

    Enter the following commands one at a time and press enter:

          AmpRes 3  
          VoltRes 3  
          EnergyRes 3  
          WattRes 3  
          FreqRes 3  
          SetOption21 1
          VoltageSet 230

    Enter the command PowerSet XXX with XXX replaced by the wattage specified for the test load (e.g., "40" for a 40W light bulb).

    Click "Main Menu".

    The main page now should show correct power readings with several decimals precision.

 - (5) *MQTT Broker Setup*

    At present, the only known way to achieve high-frequency automatic readouts is by polling over MQTT. This is not ideal and needs additional setup, unfortunately.

    If you happen to have a MQTT Broker around already, skip to step (6); otherwise, you need to set one up. The below scenario assumes Mosquitto is packaged for your GNU/Linux distribution (and therefore does not configure any security), so only do this in your own trusted network and switch it off when not needed.

   - install the `mosquitto` package
   - add a file `/etc/mosquitto/conf.d/listen.conf` with the following content:

          listener 1883  
          allow_anonymous true

   - start Mosquitto using `systemctl start mosquitto.service`

 - (6) *MQTT Tasmota Setup*

    Connect to the Tasmota device using a web browser, and open the MQTT configuration page via Configuration > Configure MQTT.
    
    Enter the IP address of the MQTT broker into the "Host" field.

    Note down the value shown right of the "Topic" label in parentheses (typically something like "tasmota_xxxxxx"). This will be needed later on to address the device via MQTT. You can also change the default value to something easier to remember, but this has to be unique if you have multiple devices.

    Click "Save".

    The device will restart and once it is back you should see output in its Console prefixed with "MQT".

 - (7) *Verifying MQTT Communication*

    This assumes you have the Mosquitto client tools installed, which are usually available as distribution packages.

    You need two terminals to verify that MQTT communication works as intended.

      - In terminal 1, run `mosquitto_sub -t 'stat/<topic>/STATUS10'`
      - In terminal 2, run `mosquitto_pub -t 'cmnd/<topic>/STATUS' -m '10'`

    Replace `<topic>` with the value noted down in step (6).

    Everytime you run the second command, you should see a set of values printed in the first terminal.

 - (8) *Continuous Power Measurements*

    See [these scripts](https://volkerkrause.eu/2020/10/17/kde-cheap-power-measurement-tools.html).

 - (9) *Switching WiFi Networks*

    For security reasons, once connected to a WiFi network, Tasmota will not let you get back to step (2) by default without hard resetting the device (40-second button press). However, a hard reset also removes all settings and the calibration. If you need to move to a different network, there are less drastic options available, but these changes can only be made inside the network you originally connected to:

    Under Configuration > Configure WiFi, you can add details for a second WiFi access point. Those will be tried alternatingly with the first configuration by default. This does not compromise security, but requires you to know the details for the network you want to connect to.

    You can configure Tasmota to open an access point as in step (2) by default for a minute or so after boot, and then try to connect to the known configurations. This makes booting slower in known networks, and opens the potential for hijacking the device, but it can be convenient when switching to unknown networks. This mode can be enabled in the Console by the command `WifiConfig 2`, and disabled by the command `WifiConfig 4`.

    For Tasmota version 11 the 40-second button press reset can leave the device in a non-booting state, whereas resetting from the Console using `Reset 1` doesn't have that problem, but has to be done before disconnecting from the known WiFi as well.

 - (10) *Recovering Non-Booting Devices*
 
    First and foremost: **DO NOT CONNECT THE DEVICE TO MAIN POWER**! That would be life-threatening. The entire flashing process is solely powered from 3.3V supplied by the serial adapter. Do not do any of this without having read this [getting started guide](https://tasmota.github.io/docs/Getting-Started/).

    With Tasmota 11, you can end up in a non-booting state by merely resetting the device using the 40-second button press. This does not permanently damage the device, and it can be fixed with reflashing via a serial adapter.

    The basic process is described in the above [guide](https://tasmota.github.io/docs/Getting-Started/). The PCB layout of the Gosund SP 111 can be seen [here](https://templates.blakadder.com/gosund_SP111_v1_1).

    In order for this to work, you need to connect GPIO0 (second pin on bottom left in the above image) to GND **before** powering up (i.e., before connecting with USB). The device LEDs (red and blue) are a useful indicator of whether you ended up in the right boot mode: the red LED should be on, and not flashing quickly, and the blue and red LED should not be on together. Once in that state, the connection can be removed (e.g., if you just hold a jumper cable to the pin) and it will remain in the right mode until a reboot.

    Again: **DO NOT CONNECT THE DEVICE TO MAIN POWER** as this is life-threatening.

## (B) Hardware Operating Life

The criteria in category (B) ensure that the software has low-enough performance requirements to run on older, less powerful hardware at least five years old.

Many FOSS applications run on hardware much older than 5 years. In fact, members of the KDE community have noted that KDE's desktop environment `Plasma` runs on hardware from even 2005!

This category is relatively easily to fulfill for the Blue Angel application. Compliance entails a declaration of backward compatibility, including details about the hardware on which the software runs and the required software stack. To demonstrate compliance, document the following information in two documents called "Annex 1" and "Annex 2":

 - *Reference System Year* &mdash; e.g., 2015

 - *Model* &mdash; e.g., Fujitsu Esprimo 920

 - *Processor* &mdash; e.g., Intel Core i5-4570

 - *Cores* &mdash; e.g., 4

 - *Clock Speed* &mdash; e.g., 3,6 GHz

 - *RAM* &mdash; e.g., 4 GB

 - *Hard Disk (SSD/HDD)* &mdash; e.g., 500 GB

 - *Graphics Card* &mdash; e.g., Intel Ivybridge Desktop

 - *Network* &mdash; e.g., Realtek Ethernet

 - *Cache* &mdash; e.g., 6144 KB

 - *Mainboard* &mdash; e.g., Fujitsu D3171-A1

 - *Operating System* &mdash; e.g., Ubuntu 18.04

Again, examples for Okular can be found at the following links:

 - [Annex 1](https://invent.kde.org/teams/eco/blue-angel-application/-/blob/master/applications/okular/de-uz-215-eng-annex-1-okular.docx)
 - [Annex 2](https://invent.kde.org/teams/eco/blue-angel-application/-/blob/master/applications/okular/de-uz-215-eng-annex-2-okular.xlsx)

## (C) User Autonomy

As discussed in PART II, the Blue Angel user autonomy criteria cover eight general areas:

  1. Data Formats
  2. Transparency
  3. Continuity Of Support
  4. Uninstallability
  5. Offline Capability
  6. Modularity
  7. Freedom From Advertising
  8. Documentation

Many FOSS projects may take for granted that Free Software respects user autonomy and in some cases information from the above list is missing from websites, manuals, wikis, etc. This may include documentation about support for open standards, uninstallability, continuity of support, and so on.

Documenting this information is important, both for fulfilling the Blue Angel award criteria and for giving users information about long-term sustainable use of their software and hardware.

This is not an exhaustive presentation for each of the above categories of the Blue Angel criteria. Rather, this guide focuses on aspects of the criteria which KDE/FOSS projects can easily document and provide (which is already most of the work). For the full criteria, see Section 3.1.3 in the [basic award criteria](https://www.blauer-engel.de/en/productworld/resources-and-energy-efficient-software-products).

### 2.1 Data Formats

The main information to include in documentation:

 - Which (open) data formats are supported&mdash;with links to specifications, e.g., [PDF](https://www.iso.org/standard/51502.html)?

 - Also of interest: Are there examples of other software products that process these data formats?

For an example of the online documentation of supported data formats for Okular, visit [the Okular website](https://okular.kde.org/formats/).

An example of documentation for the Blue Angel can be found in [Annex 4](https://invent.kde.org/teams/eco/blue-angel-application/-/blob/master/applications/okular/de-uz-215-eng-annex-4-okular.md).


### 2.2 Transparency Of The Software Product

When missing, provide links to documentation of the API, source code, and software license. For example, for KMail:

 - [KDE PIM API documentation](https://api.kde.org/kdepim/index.html)

 - [Source code](https://invent.kde.org/pim/kmail)

 - [License](https://invent.kde.org/pim/kmail/-/blob/master/LICENSES)

An example of documentation for the Blue Angel can be found in [Annex 5](https://invent.kde.org/teams/eco/blue-angel-application/-/blob/master/applications/kmail/de-uz-215-eng-annex-5-kmail.md).


### 2.3 Continuity Of Support

Details about continuity of support to document include:

 - Information about how long the software has been supported for (with links to release announcements).

 - Release schedule and details (e.g., who maintains the software).

 - Statement that updates are free of charge.

 - Declaration on how the free and open source software license enables continuous support indefinitely.

 - Information about whether and how functional and security updates may be installed separately.

An example of Okular's continuity of support documentation for the Blue Angel can be found in Section 3.1.3.3 of [Annex 6](https://invent.kde.org/teams/eco/blue-angel-application/-/blob/master/applications/okular/de-uz-215-eng-annex-6-okular.md).

### 2.4 Uninstallability

How are users able to completely uninstall the software? Relevant details might include:

 - Uninstallation instructions that depend on how the software was installed (source code or binary).

 - Examples of uninstallation instructions (source code or package managers, with relevant links to documentation).

 - Information about whether user-generated data is also removed when uninstalling a program.

An example of Okular's uninstallability documentation for the Blue Angel can be found in Section 3.1.3.4 of [Annex 6](https://invent.kde.org/teams/eco/blue-angel-application/-/blob/master/applications/okular/de-uz-215-eng-annex-6-okular.md).

### 2.5 Offline Capability

Does the software require external connections such as a license server in order to run? If not, and no network connection is needed as the software can be used offline, this should be documented.

An example of Okular's offline capability documentation for the Blue Angel can be found in Section 3.1.3.5 of [Annex 6](https://invent.kde.org/teams/eco/blue-angel-application/-/blob/master/applications/okular/de-uz-215-eng-annex-6-okular.md).

### 2.6 Modularity

Information to document includes:

 - What aspects of the software are modular and can be deactivated during installation?

 - Can the software manuals or translations be installed separately?

 - Are any modules which are unrelated to the core functionality included with installation, such as tracking modules or cloud integration? If not, document it!

An example of Okular's modularity documentation for the Blue Angel can be found in Section 3.1.3.6 of [Annex 6](https://invent.kde.org/teams/eco/blue-angel-application/-/blob/master/applications/okular/de-uz-215-eng-annex-6-okular.md).

### 2.7 Freedom From Advertising

If the software does not display advertising, make this explicit in manuals and wikis and declare it in the Blue Angel application document.

### 2.8 Documentation

This includes the following:

 - General process for installing/uninstalling the software? This may include generic instructions or tutorials for a specific desktop environment or package manager.

 - Data import/export process?

 - What can users do to reduce the use of resources (e.g., configuration options for improving performance)?

 - Does the software have any resource-intensive functionality not necessary for the core functionality? If not, great. Let's tell the users!

 - Licensing terms related to further development of the software products, with links to source code and license?

 - Who supports the development of the software?

 - Does the software collect any personal data? Is is compliant with existing data protection laws? If yes, document it!

 - What is the privacy policy? Is there telemetry? If yes, how does the software handle data security, data collection, and data transmission? Also, are there ads or tracking embedded in the software? If not, excellent&mdash;now make sure to spread the word!

An example of Okular's product documentation for Blue Angel certification can be found in Section 3.1.3.8 of [Annex 6](https://invent.kde.org/teams/eco/blue-angel-application/-/blob/master/applications/okular/de-uz-215-eng-annex-6-okular.md).

## Submitting To RAL

For examples of all of the above documentation, see KDE's [Blue Angel Applications](https://invent.kde.org/teams/eco/blue-angel-application) repository.

Once you have all of the documentation prepared, you need to submit it for review to RAL gGmbH (if you recall, RAL is the authorized body that assesses compliance with the award criteria). The portal for submitting Blue Angel applications can be found [here](https://portal.ral-umwelt.de/) (https://portal.ral-umwelt.de/).

If you need help with the online interface, RAL provides [documentation](https://portal.ral-umwelt.de/RALUmwelt/Anleitung/1).

## Example Submission Documents

Below are examples of Blue Angel documentation for Okular.

 - [Annex 1: Form](https://invent.kde.org/teams/eco/blue-angel-application/-/blob/master/applications/okular/de-uz-215-eng-annex-1-okular.docx)

 - [Annex 2: Spreadsheet](https://invent.kde.org/teams/eco/blue-angel-application/-/blob/master/applications/okular/de-uz-215-eng-annex-2-okular.xlsx)

 - [Annex 3: OSCAR Report For Idle Mode](https://invent.kde.org/teams/eco/blue-angel-application/-/blob/master/applications/okular/de-uz-215-eng-annex-3-okular-idle.pdf)

 - [Annex 3: OSCAR Report For SUS](https://invent.kde.org/teams/eco/blue-angel-application/-/blob/master/applications/okular/de-uz-215-eng-annex-3-okular-scenario.pdf)

 - [Annex 4: Data Formats](https://invent.kde.org/teams/eco/blue-angel-application/-/blob/master/applications/okular/de-uz-215-eng-annex-4-okular.md)

 - [Annex 5: Open Standards](https://invent.kde.org/teams/eco/blue-angel-application/-/blob/master/applications/okular/de-uz-215-eng-annex-5-okular.md)

 - [Annex 6: Product Information](https://invent.kde.org/teams/eco/blue-angel-application/-/blob/master/applications/okular/de-uz-215-eng-annex-6-okular.md)

 - [Annex 7: Data format for passing on the product information about resource and energy efficiency](https://invent.kde.org/teams/eco/blue-angel-application/-/blob/master/applications/okular/de-uz-215-eng-annex-7-okular.xml)

## Notable Sustainable Software Initiatives

There are many initiatives working on tooling for measuring the energy consumption of software. We would like to mention five in particular who have been collaborating with the KDE Eco initiative:

 - [The Green Software Engineering work group](https://www.umwelt-campus.de/en/green-software-engineering) at the [Environmental Campus Birkenfeld](https://www.umwelt-campus.de/en/) (German: *Umwelt Campus Birkenfeld*)
 
   Since 2008, the Green Software Engineering work group have been working on research projects with a focus on sustainable software. Their research provides the foundation of the work here, and their team developed tools such as `OSCAR` and have measured various KDE applications, including Okular.

 - [Öko-Institut e.V.](https://www.oeko.de/en/)
 
   The Öko-Institut is one of Europe's leading independent research and consultancy organizations working for a sustainable future. The *Sustainable Products & Material Flows* research group is working on various measurement methodologies. In this [blog post](https://blog.oeko.de/energieverbrauch-von-software-eine-anleitung-zum-selbermessen/) (in German) researchers present a self-measurement technique using a simple Python script.

 - [Green Coding Berlin](https://www.green-coding.berlin/)
 
   Green Coding Berlin is focused on research into the energy consumption of software and its infrastructure, creating open source measurement tools, and building a community and ecosystem around green software. 

 - The [SoftAWERE](https://sdialliance.org/steering-groups/softawere) project from the [Sustainable Digital Infrastructure Alliance](https://sdialliance.org/)
 
   The SoftAWERE steering group oversees and sets the direction for the development of tools and labels for energy-efficient software applications.

 - [Green Web Foundation](https://www.thegreenwebfoundation.org/)
 
   The Green Web Foundation tracks and accelerates the transition to a fossil-free internet.



