# Sustainable Events Checklist

This is a lightly-modified checklist based on *Guidelines on Sustainable Event Organisation*, BMUV/UBA, 2020

https://www.bmuv.de/en/publication/guidelines-on-sustainable-event-organisation

All criteria are formulated as a yes-no question. Yes-answers indicate a potentially more sustainable option.

## Checklist 1: Transport

### Avoiding transport-related environmental impacts
- [-] 1.1 Are there video streams for remote participants? Are recordings made available after the conference?

### Travel to and from the venue
- [-] 1.2 Is the location accessible with public transportation?
- [-] 1.3 Do start/finish times of the event permit the use of public transportation?
- [-] 1.4 Are participants well-informed about environmentally friendly transportation options?
  + Is information provided in announcements, including train and public transport timetables, etc.?
  + To compare CO2 emissions of transportation options, see: https://www.umweltmobilcheck.de/
- [-] 1.5 Are there incentives for participants to use public transportation (e.g., public transportation is included in the ticket)?

### Mobility at the conference venue
- [-] 1.6 During the event, is information about public transportation provided?
   + Is info easy to find at website? In lanyards?
   + Note: KDE apps [KTrip](https://apps.kde.org/ktrip/) or [KDE Itinerary](https://apps.kde.org/itinerary/).
- [-] 1.7 Are distances kept short? Is there the possibility of a cooperative arrangement for local bike rental?
- [-] 1.8 When needed, can low-emission vehicles (e.g., EVs) be used?
- [-] 1.9 Is there the possibility for shared transportation for group travel between locations?
- [-] 1.10 Are drivers trained for "fuel-efficient driving"?

### Climate-neutral mobility
- [-] 1.11 Have unavoidable transport-related greenhouse gases been estimated and offset through high-quality climate projects?
  + See: https://www.dehst.de
  + See: https://www.umweltbundesamt.de/publikationen/freiwillige-co2-kompensation-durch

### Venue infrastructure
- [-] 1.12 Are bicycle parking facilities available?
- [-] 1.13 Is there adequate signposting indicating the location of the nearest public transport stop or station?
- [-] 1.14 Are there parking systems for cars/buses to arrive directly to parking spaces?
- [-] 1.15 Have the required number of parking spaces been estimated? Are existing parking spaces used?

### Traffic management
- [-] 1.16 Are there shuttle service with low-emission vehicles to and from overflow parking spaces?
- [-] 1.17 Is there designated parking spaces for different user groups (e.g., for people with disabilities, carpools, electric vehicles, car shares)?
- [-] 1.18 Are there estimates of visitor and traffic flows (by transport mode)?
- [-] 1.19 Is there separate routing for emergency vehicles, press, VIPs, visitors, etc.?
- [-] 1.20 Is overflow parking provided?
- [-] 1.21 Are there signs to guide traffic or car park guidance systems?
- [-] 1.22 Are there pay car parks?
- [-] 1.23 Are there signs indicating non-parking areas or barriers and access restrictions?
- [-] 1.24 Are speed limits imposed around the event area?

## Checklist 2: Venue and accommodation

### Selection Of Hotels & Venue
- [-] 2.1 Are hotels and venues selected that have an environmental management system?
  + Hotels and venues should be validated and registered according to EMAS.
  + Hotels that fulfill environmental criteria may also have the EU Ecolabel.
  + See: https://www.emas-register.de
  + See: https://ec.europa.eu/environment/emas/
takeagreenstep/
  + See: http://ec.europa.eu/
- [-] 2.2 Have hotels been considered with respect to sustainability aspects?
  + See: https://www.bookdifferent.com/en/
- [-] 2.3 Have all other measures been met, such as mobility, energy and climate, temporary structures, procurement of products and services, catering, waste management, water use and accessibility?
  + Particularly important are good railway and local trans­port connections.

### Side events
- [-] 2.4 Are the venue and accommodation as close to each other as possible?
- [-] 2.5 Are participants travelling by public transport or rental bike? By low-emission shuttle buses or using car sharing, when possible?
- [-] 2.6 Have environmental and social criteria been considered when planning side events?

## Checklist 3: Energy and climate

### Building, energy consumption
- [-] 3.1 Was energy consumption considered when choosing conference buildings and hotels, using EMAS registration or the EU Ecolabel as guidance?
- [-] 3.2 Are meeting and conference rooms heated above 20 degrees?
- [-] 3.3 Is the thermostat set to lower temperatures during the night (heating)?
- [-] 3.4 Is the building cooled no more than 6 degrees below the outside temperature?
- [-] 3.5 Is daylight, optimised lighting control, optimised sun protection and energy-efficient lighting used?
- [-] 3.6 Are rooms ventilated in a way that is energy-efficient?
- [-] 3.7 Is electricity obtained from renewable sources (such as green energy suppliers)?

### Climate-neutral events
- [-] 3.9 Is the carbon footprint of the event calculated and offset by mitigation measures else­where, using high-quality climate action projects (see measure 1.11 in the checklist for the mobility action area)?
  + See: https://www.dehst.de/EN/home/home_node.html
  + See: https://www.umweltbundesamt.de/en/publikationen/voluntary-co2-offsetting-through-climate-protection

## Checklist 4: Temporary structures and stands

### Venue and delivery logistics
- [-] 4.1 Does the venue not require the assembly of temporary structures?
- [-] 4.2 Are access routes and the number of deliveries of goods and materials optimized?
- [-] 4.3 Are the different types of waste collected and disposed of separately?
- [-] 4.4 Is waste avoided by using reusable or at least recyclable transport packaging?
- [-] 4.5 Is there a plan for reuse of materials?
  + For example, storage for future events, hiring or leasing large components, or donating items.
- [-] 4.6 Are systems modular? (For temporary structures?)
- [-] 4.7 Are construction materials used as well asversions of fittings that are durable and recyclable and do not have adverse effects on the environment and health?
  + See original document for further resources.
- [-] 4.8 Is a soil protection system installed for pathways and surfaces?

## Checklist 5: Procuring products and services

### Sustainable procurement in general
- [-] 5.1 Can the procurement be avoided?
- [-] 5.2 Can used products or rented products be used instead?
- [-] 5.3 Can products and services be procured that have been awarded an ecolabel (Blue Angel, EU Ecolabel) or that fulfil their criteria?
  + If there are no environmental labels for the products to be procured, research the relevant environmental criteria elsewhere (see original PDF).
  + Also consider social aspects (such as Fairtrade flowers).
- [-] 5.4 Have life cycle costs been considered?
  + See https://www.umweltbundesamt.de/en/topics/economics-consumption/green-procurement/life-cycle-costing
  + Information and tools, including an introductory course on calculating life cycle costs and how to use them in the procurement process).
- [-] 5.5 Have environmental management expertise been considered?
  + Evidence in the form of an environmental management system (such as EMAS)
  + EMAS in public procurement guidelines: https://www.umweltbundesamt.de/en/publikationen/

### Paper
- [-] 5.6a Are digital invitation and event management systems used?
  + Send invitations and meeting documents electronically.
  + Make use of the conference website and conference app.
  + Display the agenda and notices only on screen during the conference.
- [-] 5.6b Are QR codes used to distribute brochures?
  + Set out a few paper brochures for people to look at and include QR codes in them.
- [-] 5.6c Are smaller formats used and copies printed on both sides?
  + Inform staff that handouts must be printed on both sides.
- [-] 5.6d Has the precise number of participants been determined?
- [-] 5.7 Is used, eco-certified recycled paper used?
- [-] 5.8 Is the following used: Recycled paper, maximum 100% (including UV portion) according to DIN ISO 2470 and maximum 135 according to ISO standard 11475 (CIE whiteness)?

### Batteries
- [-] 5.9 Are rechargeable batteries (no nickel cadmium batteries) used?
  + Ensure dead batteries are recycled properly.

### Cleaning products
- [-] 5.10 Are cleaning products used sparingly?
  + clarify this issue with cleaning personnel
- [-] 5.11 Have environmentally-compatible, eco-certified cleaning products been purchased?

## Checklist 6: Catering
- [-] 6.1 Are organic products and Fairtrade prod­ucts such as coffee, tea and juices offered?
  + Practical guide "Mehr Bio in Kommunen" (More organic food in municipalities) https://www.biostaedte.de/images/pdf/leitfaden_V4_verlinkt.pdf
  + https://www.forum-fairer-handel.de/startseite/
  + www.fairtrade-deutschland.de
- [-] 6.2 Are seasonal produce used and foods transported in ways that are environmentally sound?
  + Do not use products from heated greenhouses or foods transported by air.
  + https://www.verbraucherzentrale.de/wissen/lebensmittel/gesund-ernaehren/saisonkalender-obst-und-gemuese-frisch-und-saisonal-einkaufen-17229
 + Due to the ban on discriminatory practices, public-sector clients are not permitted to specify regional products in calls for tenders.
- [-] 6.3 Is vegan and vegetarian food served?
  + https://proveg.com
- [-] 6.4 If meat and/or fish must be served in exceptional cases, is organic meat products that meet high animal welfare standards choosen?
  + https://www.oekolandbau.de/en/bio-siegel/
  + https://www.tierwohl-staerken.de/einkaufshilfen/tierwohl-label/
- [-] 6.5 If fish must be served in exceptional cases, are choices from endangered stocks avoided when selecting and combining fish and fish products.
  + Make sure that procurement procedures apply criteria for certified fish (such as the independent MSC label or Naturland certification).
  + https://www.umweltbundesamt.de/umwelttipps-fuer-den-alltag/essen-trinken/fisch#textpart-2
  + www.fischbestaende-online.de
- [-] 6.6 Is tap water in carafes provided (instead of bottled water)?
  + Refill carafes with fresh water regularly.
  + Remove the (glass) carafes and wash them at appropriate intervals.
  + Use returnable bottles when serving other cold drinks.
- [-] 6.7 Are environmentally friendly paper products used?
  + See: https://www.blauer-engel.de/en/products/home-living/paper-filters-for-hot-beverages
  + See: https://www.blauer-engel.de/en/products/home-living/sanitary-papers-toilet-paper-paper-towels-handkerchiefs

### Intolerances and allergies

- [-] 6.8 Are prepared dishes labelled so foods that cause allergies or intolerances are clearly recognisable?
- [-] 6.9 Are employees given instructions for catering?

### Avoiding food waste and other waste
- [-] 6.10 Are reusable crockery, cutlery and glasses used?
  + Use reusable crockery
  + Hire mobile dishwashing units
- [-] 6.11 Are appropriate quantities of food offered?
- [-] 6.12 Is packaging used that prevents or reduces waste?
  + Choose larger containers, but verify that this will not cause additional food waste.
  + Do not offer milk or sugar in single-serve packaging.
- [-] 6.13 Are foods in a buffet labelled (to avoid "mistakes")?
- [-] 6.14 Are servers made aware of the need to serve smaller portions, or to restock the buffet only when necessary?
- [-] 6.15 Is surplus food donated?
  + To food distribution charities, if permissible under applicable hygiene provisions.

### Waste separation
- [-] 6.16 Is kitchen waste collected separately?
- [-] 6.17 Is used cooking oil from deep fat fryers collected separately?

### Catering service providers
- [-] 6.18 When selecting a catering partner, was their compliance with the quality standards of the German Nutrition Society (DGE) checked?
  + https://www.jobundfit.de/dge-qualitaetsstandard/

## Checklist 7: Waste management
### Waste avoidance
- [-] 7.1 Was waste from packaging minimized?
  + By using reusable packaging and ordering products in large containers if their entire contents will be used.
  + See: https://www.blauer-engel.de/en/products/business-municipality/returnable-transportation-packagings/returnable-transportation-packagings
- [-] 7.2 Has a take-back system be instituted and items such as name tags reused?
- [-] 7.3 Are recyclable products used and is packaging made of recycled material, such as recycled cardboard for boxes?
- [-] 7.4 Are collection points set up so that waste can be collected separately – especially paper, biowaste, glass and lightweight packaging? Were they clearly labelled (particularly at international events)?
- [-] 7.5 Are all suppliers and operating companies informed about the separation system?
- [-] 7.6 Are suppliers required to take back packaging.

### Waste management
- [-] 7.7 Is packaging waste disposed of according to the Packaging Act (Verpackungsgesetz)?
- [-] 7.8 Is other waste that has been collected separately (paper, biowaste, etc.) recycled according to the Circular Economy Act (Kreislaufwirtschaftsgesetz) and the Commercial Wastes Regulation (Gewerbeabfallverordnung)?
- [-] 7.9 Have the public waste disposal agencies disposed of residual waste in accordance with the Circular Economy Act and the Commercial Wastes Regulation?

## Checklist 8: Water use
### Water consumption
- [-] 8.1 Has water consumption been reduced, for example, by using water-saving appliances in kitchens and canteens?
- [-] 8.2 Have notices been posted drawing attention to the water-saving fixtures in washrooms in order to raise awareness among participants?


## Checklist 9: Conference swag and giveaways
### General
- [-] 9.1 Has conference swag and giveaways been avoided?

### Selecting products if necessary in individual cases
- [-] 9.2 Are environmentally sound swag and give-aways used  that have been produced in a socially responsible way
  + Choose eco-certified swag and giveaways
- [-] 9.3 If food items are purchased for gifts, are seasonal, organic products chosen that are transported in an environmentally sound way, or that are Fairtrade?
  + https://www.fairtrade-deutschland.de/
- [-] 9.4 Are retractable ballpoint pens or pencils made of cardboard or paper mâché used?
  + Alternatively, choose unpainted pencils or retractable ballpoint pens made of wood.
  + See: https://www.blauer-engel.de/en/products/paper-printing/writing-utensils-stamps
  + For wood: FSC or PEFC
- [-] 9.5 Are backpacks and bags made of environmentally compatible materials?
  + Meaningful use of them after the event should be possible.
- [-] 9.6 If German specialities are provided, have organic specialities given?
- [-] 9.7 Have seasonal flowers shipped in an eco-friendly way been presented or used? Fairtrade flowers?
  + https://www.fairtrade-deutschland.de/

## Checklist 10: Organisation, communication and evaluation
### Organisation
- [-] 10.1 Has a designated contact person been provided for specific questions related to
sustainability?
  + It is best to set up a centralised service office to handle the organisation of events (central event management) with due regard for sustainability concerns.

### Communicating goals and measures
- [-] 10.2 Hss public relations work been done to communicate the goal of holding a sustainable event and the planned measures early on (for example, with the invitation, on the conference website or in the conference app)?
  + https://unfccc.int/process-and-meetings/conferences/un-climate-change-conference-november-2017/about/sustainable-conference
- [-] 10.3 Have staff at the venue been informed about sustainability-related measures and are they involved in implementing those measures?
- [-] 10.4 Have participants been informed about sustainability issues at the event (for example, hang banners or post displays in presentation areas during breaks)?
- [-] 10.5 Is there a requirement for training external staff in calls for tenders?
  + Staff at the venue, cleaning crews, etc. should be familiarised with the essential features of the sustainability strategy.
- [-] 10.6 Have the effects of measures that were taken determined in terms of quality and quantity.
  + Collect data (such as waste volumes, consumption of paper and materials, and traffic volume).
  + Use evaluation forms (electronic if possible) to determine the perceptions and satisfaction of participants, speakers and attendees after the event.
- [-]  10.7 Have the collected data been analyzed?
  + Calculate the carbon footprint.
  + Demonstrate success.
  + Determine potential improvements for future events.
- [-] 10.8 Have the analysed data (particularly after large events) been communicated?
  + Make reporting transparent; for example, by issuing an EMAS Environmental Statement or posting on the conference website.

# TODO
- Checklist 11: Accessibility
- Checklist 12: Gender mainstreaming