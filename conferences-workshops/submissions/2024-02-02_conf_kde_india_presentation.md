## Emulation Tools for Sustainable Software Development: Simulating Real-World Usage Scenarios at KDE-Eco 

This presentation showcased the practical applications of emulation tools in sustainable software development within the KDE-Eco. Attendees learned how emulation tools such as xdotool, actiona, and KDE Eco Tester can automate tasks, streamline workflows, and assist in meeting quality standards, with a focus on preparing KDE applications for Blue Angel Eco Certification.

Through demonstrations and shared experiences, attendees gained insights into automating the creation of usage scenarios and ensuring eco-friendliness and quality standards in software development. While emulation tools offer significant benefits, considerations for their limitations and complexities were also discussed.

## Speaker bio:

Rudraksh Karpe, a final-year CS undergraduate student from India and a Season of KDE 2023 mentee under Joseph P. De Veaugh-Geiss, has a passion for sustainable software development. As a contributor to the Blue Angel Certification Preparation for KDE applications project, Rudraksh brings firsthand experience and enthusiasm to the topic of leveraging emulation tools for eco-friendly software development.

Joseph P. De Veaugh-Geiss (he/him) serves as the community manager for the KDE Eco initiative, dedicated to advancing ecological sustainability within Free Software development. With his expertise and leadership, the KDE Eco initiative strives to integrate sustainable practices into software development processes.