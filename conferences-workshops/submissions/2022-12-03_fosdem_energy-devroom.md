# Combatting Software-Driven Environmental Harm With Free Software

Digital technology is a major contributor to environmental harm, whether it is the 'tsunami' of e-waste filling landfills or the CO2 emissions on a par with with aviation industry.

Oft overlooked is that software plays a crucial role. For instance, software bloat means less powerful hardware may stop working and end up as toxic waste in landfills, while new devices are manufactured unnecessarily -- the production of which often costs more energy than the usage over their operating life. Moreover, software bloat also entails that the software will have higher energy demands and pump more greenhouse gases into the atmosphere -- in order to achieve the same result. Digital sustainability is, in many ways, a question of software.

The Blue Angel ecolabel, the official environmental label of the German government, began certifying software in 2020. In the award criteria, sustainability is closely linked with transparency and user autonomy. This connection has been one of the motivations behind the KDE Eco initiative since its beginning in 2021. In this talk I will present the environmental harm driven by software, with a focus on energy consumption. I will link the values of Free and Open Source software to sustainable software design. Finally, I will provide an overview of the work of the KDE Eco project and the new sustainability goals of KDE.

# Bio

Joseph P. De Veaugh-Geiss (he/him) is the project and community manager of KDE e.V.’s "Blauer Engel 4 FOSS" project as part of KDE Eco. He supports the project by collecting and spreading information about Blauer Engel eco-certification and resource efficiency as it relates to Free Software. The overarching goal of KDE Eco is to strengthen sustainability as part of the development of Free Software.
