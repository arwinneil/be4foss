<!--
SPDX-FileCopyrightText: 2021 KDE e.V. <https://ev.kde.org/>

SPDX-License-Identifier: CC-BY-SA-4.0
-->


# Table of Contents

1.  [Blauer Engel for FOSS (BE4FOSS)](#org134440b)
    1.  [Vision](#orga9886e1)
    2.  [Communication](#orge005ed2)
    3.  [Contributions](#orga1beb4c)
    4.  [License](#orge5e732d)
    5.  [Funding Notice](#orgb73a0f2)


<a id="org134440b"></a>

# Blauer Engel for FOSS (BE4FOSS)


<a id="orga9886e1"></a>

## Vision

KDE Eco is comprised of two projects working to build energy-efficient free software: BE4FOSS and FEEP!

"*Blauer Engel* for FOSS" (BE4FOSS) is a repository for collecting, summarizing, and spreading information related to  [*Blauer Engel*](https://de.wikipedia.org/wiki/Blauer_Engel) (['Blue Angel](https://en.wikipedia.org/wiki/Blue_Angel_(certification))') eco-certification and to resource efficiency as it relates to Free and Open Source Software (FOSS) development. One goal of BE4FOSS is to promote the work being done by the Free and open source software Energy Efficiency Project ([FEEP](https://invent.kde.org/cschumac/feep)) project, which is focused on measuring and evaluating energy consumption in software.

The overarching aim of these projects is to strengthen sustainability as part of the development of free software. Let's be pioneers in building an energy-efficient, free software future!


<a id="orge005ed2"></a>

## Communication

For discussions around the topic there is the mailing list [energy-efficiency@kde.org](https://mail.kde.org/cgi-bin/mailman/listinfo/energy-efficiency). Feel free to subscribe and post there to get involved with this and the [FEEP](https://invent.kde.org/cschumac/feep) project.

There is also a Matrix room [KDE Energy Efficiency](https://webchat.kde.org/#/room/#energy-efficiency:kde.org) for more direct ad-hoc discussions. Feel free to join there as well and have a chat about the topic.

If you have questions and would like community support, please visit our forum: <https://forum.kde.org/viewforum.php?f=334>.


<a id="orga1beb4c"></a>

## Contributions

This is a community project. Contributions are very welcome. Don't hesitate to [open issues](https://invent.kde.org/joseph/be4foss/-/issues/new) or [merge requests](https://invent.kde.org/joseph/be4foss/-/merge_requests/new).

This repository is maintained by Joseph P. De Veaugh-Geiss <joseph@kde.org>. If you have any questions or comments be in touch.


<a id="orge5e732d"></a>

## License

The content of this repository is licensed under the Creative Commons Attribution-Sharealike 4.0 International (CC-BY-SA 4.0), unless otherwise noted. See the LICENSES directory for the full texts of all relevant licenses.


<a id="orgb73a0f2"></a>

## Funding Notice

This project was funded by the Federal Environment Agency and the Federal Ministry for the Environment, Nature Conservation, Nuclear Safety and Consumer Protection (BMUV<sup><a id="fnr.1" class="footref" href="#fn.1">1</a></sup>). The funds are made available by resolution of the German Bundestag.

<img src="uba/images/bmuv.png" alt="BMU logo" width="300px"/>

<img src="uba/images/uba.jpg" alt="UBA logo" width="250px"/>

The publisher is responsible for the content of this publication.


# Footnotes

<sup><a id="fn.1" href="#fnr.1">1</a></sup> Official BMUV und UBA-Logos are send only by request at: verbaendefoerderung@uba.de
