# Minutes for the community meetup on 2023-04-12

* Start: 19:00 CEST; End: 20:00 CEST

* 6 participants [names removed to protect participant privacy]

* *Where*: BigBlueButton

* *Topic*: Continued discussion for sustainable software goal, including tasks in the workboard, SoK23 projects, and other community work (see pad notes below).

  For general information about the Sustainable Software goal, see:

  https://community.kde.org/Goals/Sustainable_Software
  
  Minutes of the meetup are at the end of this document.

## Sustainable Software Goal Information

### Community Pad Notes

#### Progress updates

* Eco Tab template (discuss with kcoreaddons and kxmlgui): <https://invent.kde.org/teams/eco/sustainable-software-goal/-/issues/2>
* SoK23 updates. See blog posts for projects Selenium, Blue Angel prep, KDE Eco tester: 
  * Nitin Tejuja: "Season Of KDE 2023 With KDE Eco: Writing Scripts In Selenium To Reproduce KdeEcoTest Script" <https://eco.kde.org/blog/2023-04-05-sok23-kde-eco-selenium-project/>
  * Rudraksh Karpe: "Adapting Standard Usage Scenario Scripts For KDE Applications: My Journey As A Season Of KDE Mentee" <https://eco.kde.org/blog/2023-03-15-sok23-kde-eco_be/>
  * Nitin Tejuja: "Season of KDE 2023 With KDE Eco: Setting Up Selenium For Energy Consumption Measurements" <https://eco.kde.org/blog/2023-03-06-sok23-kde-eco/>
* GSoC proposal ("Remote access to the lab")
* Lab hardware ordered (see pp. 34-35 of Award Criteria recommended hardware for 2017): <https://invent.kde.org/teams/eco/sustainable-software-goal/-/issues/22> 
  * Manufacturer: Fujitsu
  * Model: Esprimo P957
  * Processor: i5-6500
  * Cores: 4
  * Clock speed: 3.2 GHz
  * RAM: 8 GB
  * Hard disk: SSD
  * Graphics card: Intel HD Graphics 530
  * The only difference with the Blue Angel parameters is I ordered the SSD to have 256 GB instead of the recommended 128 GB.
* Energy and power measurements using performance counters 
  * <https://github.com/intel/pcm>
  * <https://github.com/powerapi-ng>
  * <https://github.com/LLNL/variorum>
* Anything else you are working on?

## To-dos and previous ideas

* Initiative: Collecting info about unsupported hardware that runs Plasma well
* Initiative: KDE Eco badge (internally-defined criteria)
* Initiative: KDE Sustainable Software Dashboard / Progress dashboard (to track what has been measured, certified, etc., with informative visualizations)
* Initiative: Eco widget for Plasma (provides users: eco, power grid mix, etc. info)
* Standard frameworks have an example (a UI design mockup would be a good start)
* Campaign: upcycling with Plasma?
* KUserFeedback: mean age of hardware running software, oldest hardware, etc.
* OpenGL version, parameters of screen setup (size, number of screens)
* Add age of mainboard, CPU, etc.?
* Metrics: User engagement with the project
* Outreach to possible natural allies (e.g., GreenPeace, WWF)
* Initiative: Measurement labs (see handbook above)
* Standards and tools for measurements (making measurements comparable)
* Measurements as part of our CI?
* Introduce hamsters in a wheel as unit for energy consumption
* Any other ideas you have?

#### Previous Community Pad Notes

Included in minutes from past KDE Eco meetups at: <<https://invent.kde.org/teams/eco/be4foss/-/tree/master/community-meetups>>

#### Work Board

* GitLab Board: <https://invent.kde.org/teams/eco/sustainable-software-goal/-/boards>

  Feel free to create issues with ideas or things you want to work on.

  Feel also free to work on issues you are interested in. Just assign yourself to the card and comment there what you are doing.

#### Other Material

* KDE goals page: <<https://kde.org/goals/>

* Wiki page for Sustainable Software goal: <https://community.kde.org/Goals/Sustainable_Software>

* Presentation at Goal Kick-Off: <https://invent.kde.org/teams/eco/sustainable-software-goal/-/blob/master/presentations/sustainable-software-goal-kickoff.pdf>

* Original goal description: <https://phabricator.kde.org/T15676>

## Minutes

* Introductions

### C(omments)/Q(uestions)/A(nswers)

- C: Presentation of SoK projects
    + Selenium
    + Kde Eco Test
    + Log Formatter tool
- C: Internal power measurements (e.g., https://github.com/intel/pcm)
    + C: High resolution is good, but not capturing everything, problematic for idle mode measurements (constant activity).
    + Q: Integration into CI pipeline?
    + A: Yes, but issue of idle mode measurements, since the machine never really goes idle, cannot be measured on the same system.
    + Q: How can we bring these tools into KDE development?
    + A: Checking when software has particularly high energy consumption; write some code to get these working.
- Q: How to contribute to project?
    + C: I generally learn by building and breaking something, then fixing it.
    + C: Overwhelming amount of information, where to start.
- C: Where to write about recent activity: Awesome list of sustainable software? Presentation at parliament?
    + A: Planet, new Forum (discuss.kde.org), contact with Promo team.
    + C: What about using ChatGPT to spread information?
    + C: Energy consumption issues, transparency about energy consumption is needed, as well as energy mix.
    + C: IETF draft for adding energy consumption of API requests, is the overhead worth the gain <https://datatracker.ietf.org/doc/draft-martin-http-carbon-emissions-scope-2/>
    + C: But it is importat to provide information in order to change minds.
- Wrap up: 3 take-aways
    1. Looking into the channels of communication for KDE Eco writeups (Eco blog/Planet, Forum, etc.)
    2. Request for feedback on Eco tab: https://invent.kde.org/teams/eco/sustainable-software-goal/-/issues/2
    3. Looking into Green Metrics Tool for internal power consumption measurementsr related to performance counters linked to at pad above.