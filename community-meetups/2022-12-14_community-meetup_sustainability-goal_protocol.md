# Minutes for the community meetup on 2022-12-14

* Start: 19:00 CET; End: 20:00 CET

* 11 participants [names removed to protect participant privacy]

* *Where*: BigBlueButton

* *Topic*: Next steps for Sustainable Software goal with champion Cornelius Schumacher, open discussion

* *Details*: Read more about KDE's goals for the next two years here:

    https://kde.org/goals/

  You can find the sustainable software slides from the KDE Goals Kickoff meeting here:

  https://invent.kde.org/teams/eco/be4foss/-/blob/master/community-meetups/2022-11-28_sustainable-software-goal-kickoff.pdf

## Sustainable Software Goal Information

* GitLab Board: https://invent.kde.org/teams/eco/sustainable-software-goal/-/boards

  Feel free to create issues with ideas or things you want to work on.

  Feel also free to work on issues you are interested in. Just assign yourself to the card and comment there what you are doing.

* KDE goals page: https://kde.org/goals/

* Wiki page for Sustainable Software goal: https://community.kde.org/Goals/Sustainable_Software

* Presentation at Goal Kick-Off: https://invent.kde.org/teams/eco/sustainable-software-goal/-/blob/master/presentations/sustainable-software-goal-kickoff.pdf

* Original goal description: https://phabricator.kde.org/T15676

## Community Pad Notes

Pad link: https://collaborate.kde.org/s/cactBt4frrfTjbW

* What should be the key activities to keep Eco initiative organized and focused?
* KDE Eco badge (internally-defined criteria)
* Eco-certify more KDE software (Kate, Gcompris)
* Initiative: KDE Sustainable Software Dashboard / Progress dashboard (to track what has been measured, certified, etc., with informative visualizations)
* Add "Eco" tab to info boxes (discuss with kcoreaddons and kxmlgui) 
    * Standard frameworks, have an example (a UI design mockup would be a good start)
* Eco widget for Plasma (provides users: eco, power grid mix, etc. info)
* Campaign: old hardware that runs Plasma well 
    * KUserFeedback: mean age of hardware running software, oldest hardware, etc. 
        * OpenGL version, parameters of screen setup (size, number of screens)
        * Add age of mainboard, CPU, etc.?
    * User engagement with the project
    * Screen sizes, etc.
* Outreach to possible natural allies (e.g., GreenPeace, WWF)
* Developing a portal for measuring energy consumption of softwares remotely
* Initiative: Measurement labs 
    * Standards and tools for measurements (making measurements comparable)
    * Do concrete measurements
    * Measurements as part of our CI?
* Initiative: Showing sustainability info in KDE apps
* Introduce hamsters in a wheel as unit for energy consumption

## Minutes

* Official kickoff last week, slides linked above
    * Goal: to promote sustainability in the free software community, finding ways how we can improve sustainability
    * Gitlab board linked above
    * What are your ideas, views, what do you want to be happening? Let’s have an open discussion!
* Put eco-certification info into eco-tab in the infobox
    * Need help from KXmlGui/KCoreAddons group, one volunteer
    * Request for a UI design mockup
* Improve the measurement lab, have standard usage scenarios, measuring devices, methods; it would be interesting to show energy efficiency or carbon footprint; we would need ti find out what data to show and how to implement it technically
    * One community member thinking of making a portal
        * Developers can put in the standard usage scenario for the application, remotely connect to lab, report sent
        * Lab at KDAB could allow remote accessibility, move hardware to server room?
        * Security could be an issue, need a completely separate network?
        * Connect with a raspberry pie? Turn on remotely?
            * Turning on already works when in the network
    * We can run automated use cases and try different setups to compare differences in consumption
* Dashboard portal ideas
    * Define our own criteria and what to show, energy measurement data e.g.     * What do we show in the eco tab
    * Except for Okular, no ecocertified software yet 
    * Documentation about standard formats, transparency of the development process, privacy policies, measurement data
    * Could we gather stats of the hardware of people running the software on? Could be good for user engagement
    * Could be a promotion campaign, keeping old hardware in use is environment-friendly
    * Discussion about how to raise awareness, give people tangible information:
        * Different modes use energy different, how to get data about that and make it accessible
        * An Eco configuration with default settings defined
* Powed grid mix: is it possible to get this data?
    * Frauenhofer Institut Freiburg publish a map with data about that
    * Some scripts to access this information is available and open-source licensed
    * When renewables are higher, schedule tasks to reduce carbon footprint
    * How big of a difference would this really make?
* Wrapping up:
    * Continue the conversation at monthly (2nd Wednesday) Eco meetups, open discussion
    * OwnCloud interested in measuring, measured a lot already
        * Data relevant for the FEEP project
    * 3 takeaways: set up Eco tab via KCoreAddons and KXmlGui, contact KDAB IT re remote access to lab, OwnCloud to present at next meetup