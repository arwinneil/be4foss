Minutes for the call with the community on 2022-05-11.

Start: 19:00 CEST
End: 20:00 CEST

10 Participants + 2 BE4FOSS organizers [names removed to protect participant privacy]

Notes:

* Intro to the presenter and topic.
* Topic Part 1: Common misconceptions when assessing a computer's energy demand
* Presentation: 2022-05-11_community-meetup_presentation_Koehler_EnergyMeasureMisconceptions.pdf
    * Issue 1: Computer is more than a box and a screen. Different compute devices have different power draws and different efficiency.
    * Issue 2: What is measured is not always right. Difference between: External/On-Board/On-Chip measurements as well as physical and logical measurements. Problems could be: non-equidistant sampling, sampling noise, too small frequency.
        *  Advice -> measure again and with another device!
    * Issue 3: Faster program = less energy consumed. Shortest execution time may have high energy consumption, sweet spot may be somewhere in the middle.
    * Takeaways: 1. Think about your involved computing hardware components. 2. Measure, remeasure and compare. 3. Don’t let execution time fool you.
* Topic Part 2: Running Average Power Limit facilities (RAPL)
    * Available for Intel platforms, registers capture cumulative energy consumption (not power draw) at ~1ms resolution
    * Accessible via MSRs, Linux sysfs or perf_event_open; semi-comatible AMD implementation since Ryzen Gen 3
    * Package power plane contains core power plane and graphics power plane, next to it DRAM power plane
    * NVIDIA Jetson TX2: two triple-channel INA3221 power monitors
        * Report averaged power draw, voltage and current
        * Estimated 5% sample accuracy
        * I2C exposed via Linux sysfs-interface at sys/bus/i2c/drivers/ina3221x/*/iio_device/in_power
    * Microchip MCP29F5
        * Dual-channel power measurement device; intercepts a system’s power supply, exports data via USB
        * 200-240 Hz sampling rate (phase-locked to line frequency)
        * Up to 15 A at 230 V, 24-bit ADC, 0.5% accuracy
    * LTC2
        * Four-channel monitor for voltage, current and temperature
        * 3.3-5 V operating voltage
        * 250 Hz sampling rate, 14-bit ADC 1% accuracy
        * I2C data transfer custom build USB adapter
    * Look out for what is the maximum sampling frequency and mistake rate.
    * Demo of PinPoint.
* Links from the chat
    * https://invent.kde.org/davidhurka/usb-spi-power-sensors
    * https://lite.framacalc.org/energymeasurementtools
    * PINPOINT repo: https://github.com/osmhpi/pinpoint
    * PINPOINT Article: https://osm.hpi.de/publications/downloads/koehler2020pinpoint.pdf
    * Paper: "Measuring Energy Consumption for Short Code Paths Using RAPL"