Minutes for the call with the community on 2022-04-13.

Start: 19:00
End: 20:00

9 Participants + 1 BE4FOSS organizer/presenter [names removed to protect participant privacy]

Notes:

* Intro to the presenter and topic.
    * Topic: What have we done with KDE Eco so far, and where do we go next!
* Presentation (see 2022-04-13_community-meetup-presentation.pdf)
* See also post-discussion spreadsheet: https://lite.framacalc.org/energymeasurementtools
* Select discussion from the chat:
    * Many cheap Tasmota compatible wifi plugs can be used as power meter
    * Article comparing measurement methods: Assessing the Sustainability of Software Products - A Method Comparison, Javier Mancebo, Achim Guldner, Eva Kern, Philipp Kesseler, Sandro Kreten, Felix Garcia, Coral Calero, and Stefan Naumann
    * Use of perf mainly for "regular" performance optimizations, not to assess energy use specifically
    * For search engines, set up solr, elastic search, sepu, with indexes of different sizes (300 mb, 700 mb, 1500 mb) and ran alot of tests of it to figure out if we have differences in energy use; in general, sepu used less energy; the index didnt matter that much, at least in our tests (and with our measurements)
* Some additional links:
    * https://web.eece.maine.edu/~vweaver/projects/rapl/
    * https://github.com/osmhpi/pinpoint
