Minutes for the call with the community on 2022-02-09.

Start: 19:00
End: 20:00

10 Participants + 3 presenters + 1 BE4FOSS organizer [names removed to protect participant privacy]

Notes:

* Intro to the presenters and topic.
    * Topic: Energy consumption of distributed systems
* Presentation (see 2022-02-09_community-meetup_umwelt-campus-presentation.pdf)
    * Current Blauer Engel award criteria for desktop software
    * Expansion of the BE to distributed systems
    * Project goals & challenges of measuring energy consumption of distributed systems
    * Specific issues related to mobile applications
    * Open question: How does the FOSS community test distributed systems? What strategies are there?
* Short break to announce Okular receiving Blauer Engel!
* C(omments)/Q(uestions)/A(nswers) (not exhaustive, intended as overview)
    * C: As developer has worked on some distributed systems.
         Different dynamics than desktop software, server systems driven in a completely different way; systems used fully, e.g., overcommitment of cpu and memory to use all resources.
         Testing distributed systems is artifical, minimize complexity to get interaction as controlled as possible, not similar to what you would do in natural usage to meet user needs.
         It is hard to imagine how to isolate the different parts, e.g., when writing a usage scenario or mapping resource usage of the system, which is very interconnected to other systems, e.g., accessing databases and backup jobs not related to single use.
         What is currently done includes things like load tests, performance tests with many users.
         Can be expensive, so focus on special use cases.
         -> Do we need fundamentally different strategies to asses energy consumption of such systems?
    * C: Issue of realistic vs. artificial scenarios.
    * C: Look at cloud providers, they have biggest impact but also most challenging, don't have access to underlying physical systems. Exception is Kubernetes, which provide some information on underyling parameters in usage data, adding APIs to allow accessing this data, but information like this depends on the company.
    * C: Issue of obtaining fine-grained measurements.
    * Q to group: What does everyone think of the idea for apps of not measuring consumption within the smartphone itself, just the server. Measuring energy consumption of smartphone is difficult, hard to define what would be standard hardware.
    * C: Issue of power supply for smartphone to think about.
    * C: In the HPC area we have seen that moving data consumes more power than computing. Is this true for mobile in a similar way - network vs mobile?
    * C: Could also measure Okular, which is available as an Android app.
    * Q: For mobile, are rooted devices or emulators an option? This might give you more options towards automation and/or measurements.
    * A: Issue of measuring more the environemnt than software itself.
    * A: Also problem of functionality not being available. E.g. GPS, routing navigation did not work, and when comparing emulator and real devices network traffic was smaller in the emulator.
    * C: If the app uses native code the emulator host architecture should match, otherwise it indeed doesn't work particularly well.
    * C: FOSS chat clients for Matrix as test case, many different options (Neochat, Fractal, etc.)
    * Q: Performance/battery usage as angle to get interest?
    * A: Still difficulty with devices, i.e., they are not standardized, not easy to measure, tools potentially expensive.
    * C: May be easy to measure the client, but really hard to measure the server.
    * C: What about focusing on P2P as a way to get around measuring the server?
    * C: Re P2p could look at blockchain nodes?
    * C: One of the biggest values of this work is establishing transparency: publishing the data, having a data trail and making measurements reproducible.
    * C: Looking ahead, what seems crucial is automation of measurements, need for tooling. Ideally will be integrated into CI/CD environments.