Minutes for the call with the community on 2021-09-09.

Start: 16:00
End: 17:15

15 Participants + 2 BE4FOSS organizers [names removed to protect participant privacy]

Notes:

* BE4FOSS Organizer: Intro to the meeting, agenda, etc.
* Everyone: Round of introductions [removed to protect participant privacy]
* BE4FOSS Organizer: Presentation (see corresponding slides)
  * Blauer Engel criteria: software bloat and increased energy consumption without increase in functionality; Umwelt-Campus: bar plots to illustrate this point.
  * Presentation of two projects in KDE Eco (FEEP and BE4FOSS) as well as Blauer Engel award criteria for software.
  * Overview of the current state of KDE's Blauer Engel applications.
  * Highlight potential topics for discussion and present places where people can get involved in the projects.
* Question: Can you explain what Blauer Engel is?
  * BE4FOSS Organizer: An eco-label from Germany awarded to various kinds of products. It recently released award criteria for software.
  * Participant: Discusses upcoming projects to extend the criteria to other types of software.
  * BE4FOSS Organizer: How can community give feedback on the current criteria?
      * Participant: Ökoinstitut is analyzing why there are so few applications for the Blauer Engel seal so far. They will be sending invites for meetups, etc. People can be in touch if they are interested in participating.
* BE4FOSS Organizer: Which areas do participants here want to get involved in?
  * Participant: (related question) Is KDE going to do political advocacy?
    * BE4FOSS Organizer: FSFE is the political organisation and KDE supports it. Reach out if support is needed for a specific campaign.
  * Participant: What is your involvement in the new criteria for mobile phones?
    * Participant: Is involved and very interested in the topic because of KDE's own mobile UI Plasma Mobile.
  * Participant: Wants to explore the space so doesn't yet know what interests are. What could benefit them? Knowing the ropes and learning from projects that already went through the application process and what the obstacles are.
  * Participant: Would like to talk about how to get making energy measurements a habit, something stumbled upon during the application process. The community has made great progress but not yet established a reproducible process. How can the barrier be lowered? How can information about this be shared? This also goes into the direction of lab infrastructure and tooling as well. Would love to reach more people there. We have not reached enough people yet who can make this a regular part of their development work? How can we make developers excited about it?
    * Participant: Had focused on cheap power measurement plugs to give everyone the opportunity to test their own software with it as opposed to the official testing infrastructure used for the certification.
      * Participant: Can built-in tools be used?
        * Participant: They are not good enough usually, and not taking into account the monitor, for example.
        * Participant: The current tools are useful, but not sufficient. They are useful for individual developers to keep an eye on their application and include in regular testing.
  * Participant: FSFE is definitely interested in reaching out and getting people involved. We could make an interview when the time is right or success stories. This is also interesting for our work because it shows that Free Software is a good choice.
* BE4FOSS Organizer: Would be interested in talking more with the community about KDAB setting up a lab.
  * Participant: Happy to provide input as their team has set up a lab already. They need people to set up their own labs and check the documentation.
* BE4FOSS Organizer: Is there a conflict of interest with Umwelt-Campus being a lab for Blauer Engel certification testing since they may also be on the jury?
  * Participant: No, because those are separate parts of the organisation and they don't have anything to do with that part.
* BE4FOSS Organizer: When do we meet again and how do we focus the next meeting? Outreach or implementation?
  * Participant: The user autonomy part of the criteria is interesting. This is very easy for FOSS to fulfill but it is not documented. Things like uninstalling documentation, there are many easy-to-fill gaps there. This gardening of documentation could be a practical next meeting topic. This could also tie in with promotional activities very well to showcase privacy consciousness, etc.
  * BE4FOSS Organizer: Suggests meeting in 1 month to focus on both documentation and measurement labs.
* BE4FOSS Organizer: Encourages everyone to join the mailinglist and matrix channel to discuss further.
* Participant: Offers to do a presentation next time about Actiona, an app to record user interactions for standard usage scenarios.
  * Participant: Had a hard time working with Actiona, instead used xdotool, which unfortunately did not have a way to define mouse positions.
  * BE4FOSS Organizer: It would be relevant to do introductions to these tools in the next meeting.
  * Participant: We can also set up a date when we can show our lab and measurements.
    * BE4FOSS Organizer: Let's do that in about 2-3 weeks. I will send out a poll.
