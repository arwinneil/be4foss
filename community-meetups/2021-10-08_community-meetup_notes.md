<!--
SPDX-FileCopyrightText: 2021 KDE e.V. <https://ev.kde.org/>

SPDX-License-Identifier: CC-BY-SA-4.0
-->


# Plan (13:00 - 14:00 CEST)

-   10-15 minute presentation: Achim Guldner, Franziska Mai & Christopher Stumpf will present details of the measurement lab at Umwelt-Campus
-   5-10 minute presentation: David Hurka will provide an overview of task automation tools (e.g., Actiona) for replicating standard usage scenarios
-   25-30 minute discussion: What do we need to do to set up measurement labs for the FOSS/KDE community?
    -   KDAB will sponsor a room for a community lab. Still need to figure out details, e.g., access options, what hardware KDAB/others provide, and if this all can be done in a way that KDAB can somehow make it tax relevant.
    -   Question: Who is in Berlin and would like to help? (press b in chat) Who is outside Berlin but would travel to help? (press o) Who would be willing to set up another lab in their region? (press l)
    -   Call to action: Let's organize who can do what when to make this a reality (based on information from presentations and checklist)!
-   3-5 minute discussion: Sprint Q1 2022 in 2 parts: (i) plan usage scenarios (Kate, GCompris, other?), (ii) (finish) lab setup/measure-athon
-   3-5 minute discussion: Next meetup?
    -   Perhaps a fixed day/time ? (e.g., 3rd Wednesday/Friday of the month? 13h? or later for people who work day jobs?)
    -   Topic: Preparing Standard Usage Scenarios? (e.g., Sprint part i)


## Remarks on Standard Usage Scenarios (SUS):

-   For Blauer Engel will eventually need a text form of SUS (not just a script; for example, see <https://invent.kde.org/cschumac/blue-angel-application/-/blob/master/applications/kmail/de-uz-215-eng-annex-2-kmail.xlsx>)
-   Could someone automate translation of scripts (e.g., Actiona: <https://invent.kde.org/cschumac/feep/-/blob/master/measurements/kmail/Leerlaufszenario_KMail.ascr>) to text (above) for BE applications?


## Regarding screen size issue preparing emulation software, e.g., Actiona script, for different monitor sizes (suggestion from Jens Gröger, Öko-Institut):

-   The problem with the screen size and mouse interaction could be solved by setting a uniform screen size on the System Under Test, no matter how large the actual screen is.
-   Alternatively, you could of course save percentages instead of pixel values in the workload generator and enter the screen dimensions beforehand.
    -   The simulated mouse then transmits x=screen width \* x_percent and y=screen height \* y_percent.

