# Minutes for the community meetup on 2023-01-11

* Start: 19:00 CET; End: 20:00 CET

* 11 participants [names removed to protect participant privacy]

* *Where*: BigBlueButton

* *Topic*: Measuring the energy consumption of ownCloud

## Minutes

* Introductions
* Background to ownCloud
    * Enterprise Sync and Share, data protection
    * "ownCloud infinite scale", backend rewrite using `golang`, changes to architectural systems to overcome shortcomings of `PHP` backend
* Benchmarking became more important to show performance gains/loss, but energy consumption and efficient use of hardware are becoming more and more important
    * There are pros and cons of switching to `golang`: e.g., PHP is old but optimized; regarldess,`rust` is faster anyway
    * Need a way to measure the new system
    * Want to measure the whole setup, using real life scenarios, and have measurements be scriptable, fair, and reproducible
    * Image in slides of two servers used for testing (for maximal comparability)
    * Using `Gosund` sparkplugs to measure power, for servers SSD storage
    * k6 for testing
        * Can be programmed to make real life examples
        * Slides provided pros and cons of k6, e.g., flexible, supports HTTP-based APIs, informative output; but it only tests http APIs, not web-based or other clients
* Problems with the approach (more was provided in slides):
    * Arm architecture -- how does that influence results
    * Are the results really reflecting the effective load or is it just a spike from leaving idle mode
    * 3-second minimum for the power meter resolution
    * Saturation of network (bottlenecking)
    * knowing what is being measured?
    * timeouts due to k6
    * is k6 the right tool?

### C(omments)/Q(uestions)/A(nswers)

* Q: Why have a parallel set up with two servers? Is that to directly compare two systems?
    * A: originally to compare "infinite scale" to "nextCloud", but skeptical of the kinds of statements one could make with the results; eventually just to have two different comparisons at the same time.
    * Q: It is an interesting idea to have two systems side-by-side so you have the exact same conditions -- any issues with syncronization?
        * A: Trying to start the same k6 script on the same machine, but then the scripts are competing for bandwidth on the network; could use a better network to get around this.
* Q: The device: How was your experience using the smart powerplug with measurement capability?
    * A: It is slow, but it appears to work. Read online it should not be used with low current. Very cheap, decent API.
    * C: Device should compensate for that, phase shift between current and voltage should be compensated. Did you try to use the unofficial firmware. It can get up to 200ms resolution. Not stable though, over WIFI. 
    * Q: You used MQTT not HTTP?
    * A: Yes, only way to pull it that fast. Accuracy was quite ok, see a difference in sampling rate, but it was accurate. Even if you pull at 200ms you get integrated values. We were comparing output to high sampling rates with [Microchip MCP39F511N device](https://www.microchip.com/en-us/product/MCP39F511N) to see if we found peaks we did not expect. If this is useful, we do not know yet. But we got to high enough resolution to see individual mouse clicks and mouse moves in the data.
* Q to community: Any experience with k6?
    * A1: No, do not know the tool. It seems like it is ok.
    * A2: The K6 idea seem sane to me, You just want to understand influence of network vs. computer, and be able to differentiate between workloads (fetch files, 5 minutes of idle, ...) 
* Q: InfluxDB for further processing. How did you come to this choice?
    * A: Tried with kplot, but didn't work out for me. With this approach can start a docker container and API and it is quite easy.
* Q: Did you look into processing of the data? Similar to what OSCAR is doing? Creating reports?
    * A: This is a challenge: What statement do you want to make with the results? What data do you need to make that statement?
    * C1: Yes, this is an issue: And how do you reproduce the results? Graphs look nice, but no error bars. Assumptions can be wrong, and it would be nice to correct errors without needing to redo measurements.
    * C2: Yes, small changes can make big differences. Transparency is critical. k6 tooling is available fro all, others can use it and see what we wrote.
    * C3: The parallel set up is interesting: You can make everything directly comparable!
    * C4: We could also do the same thing twice at once to see if results are reliable
* Q to community: Does using ARM make a difference?
    * A1: I don't think we know yet, but the answer is probably messy. Relative comparison should get you quite far. But once you talk about operations that benefit from specific hardware features, then results can vary wildly. Big servers likely have accelarated hardware for cryptographic operations, for instance. We don't know yet what the impact of that is though, just speculation.
    * A2: No experience, but I think using the laptop makes more unknown noise.
    * A3: ARM-based laptops can run longer on a single battery charge than x86-based laptops.
    * A4: Agreed! ARM-based laptops also jhave more advanced power management features, such as the ability to adjust the clock speed of the processor to reduce power consumption when the workload is light.
* C: Problem of idle consumption. Idle baseload could be really high, even though an operation is low. More an issue for desktop, not so much servers.
* C: Issue of not only what the data looks like, but how you interpret it. Depends on so many factors. Probably best not to give the data to marketing.
    * Q: What would it take to get data that is marketing ready?
* C: Utilizing nodes with Kubernetes more efficiently and probably better than having a server running idle all day.
* C: Similar discussions with Blue Angel, is it for comparison or for reaching a threshold ... is it worth trying to get absolute data to compare or rather to understand your software better. Perhaps we should use measurements for relative interpretations.