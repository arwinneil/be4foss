
# Table of Contents

1.  [DAY OVERVIEW](#orgfc968c4)
2.  [LAB SETUP](#orgda2bc06)
    1.  [Hardware](#orgb82db59)
    2.  [Operating Systems](#org332017d)
    3.  [VNC Setup](#org2ce518f)
    4.  [GUDE Power Meter Measurements](#org9cc77a4)
3.  [DISCUSSION](#orgcb28c12)



<a id="orgfc968c4"></a>

# DAY OVERVIEW

<table border="2" cellspacing="0" cellpadding="6" rules="groups" frame="hsides">


<colgroup>
<col  class="org-right" />

<col  class="org-left" />
</colgroup>
<thead>
<tr>
<th scope="col" class="org-right">TIME</th>
<th scope="col" class="org-left">ACHIEVED</th>
</tr>
</thead>

<tbody>
<tr>
<td class="org-right">11:00-13:00</td>
<td class="org-left">BEGIN &#x2013; Introductions, planning, and preparation</td>
</tr>
</tbody>

<tbody>
<tr>
<td class="org-right">13:00-14:00</td>
<td class="org-left">LUNCH &#x2013; Special thanks to KDAB Berlin</td>
</tr>
</tbody>

<tbody>
<tr>
<td class="org-right">14:00-16:00</td>
<td class="org-left">COMPUTERS &#x2013; Install OSes on Computer 1 & Computer 2</td>
</tr>
</tbody>

<tbody>
<tr>
<td class="org-right">16:00-18:00</td>
<td class="org-left">VNC &#x2013; Set up computers with VNC server for remote access</td>
</tr>


<tr>
<td class="org-right">&#xa0;</td>
<td class="org-left">POWER METER &#x2013; Python script to read Gude PM with snmp</td>
</tr>
</tbody>

<tbody>
<tr>
<td class="org-right">18:00-19:00</td>
<td class="org-left">TESTING &#x2013; Testing remote access and usage scenario scripts</td>
</tr>
</tbody>

<tbody>
<tr>
<td class="org-right">19:00-20:00</td>
<td class="org-left">CONCLUSION &#x2013; Wrap up discussions, clean up, and lock up</td>
</tr>
</tbody>
</table>

-   Total of 11 participants [names removed to protect participant privacy]

-   Throughout the day several participants also worked with the high-resolution device [Microchip MCP39F511N](<https://www.microchip.com/en-us/product/MCP39F511N>) [[archived](<https://web.archive.org/web/20220524125443/https://www.microchip.com/en-us/product/MCP39F511N>)] and the low-resolution device [Gosund SP111](<https://web.archive.org/web/20201022205319/https://templates.blakadder.com/gosund_SP111_v2.html>) power plug (see also Volker Krause's [blog post](<https://web.archive.org/web/2021*/https://volkerkrause.eu/2020/10/17/kde-cheap-power-measurement-tools.html>)).


<a id="orgda2bc06"></a>

# LAB SETUP


<a id="orgb82db59"></a>

## Hardware

-   [Dell Optiplex 390](<https://www.dell.com/en-us/work/shop/desktops-all-in-one-pcs/optiplex-390-desktop/spd/optiplex-390>) [[archived](<https://web.archive.org/web/20220525113740/https://www.dell.com/en-us/work/shop/desktops-all-in-one-pcs/optiplex-390-desktop/spd/optiplex-390>)] (2011)
    -   Intel(R) Core(TM) i5-2400 CPU @ 3.10GHz 4x 3400,00 MHz
    -   Total Memory 8112044 KiB


<a id="org332017d"></a>

## Operating Systems

-   Hostname: *links*
    -   KDE Neon User Edition 64-bit
    -   neon-user-20220512-0952.iso
    -   Downloaded on 2022-05-19
    -   <https://neon.kde.org/download>
-   Hostname: *rechts*
    -   Kubuntu 22.04 64-bit
    -   kubuntu-22.04-desktop-amd64.iso
    -   Downloaded on 2022-05-21
    -   <https://cdimage.ubuntu.com/kubuntu/releases/22.04/release/>
    -   NOTE: It was necessary to run *apt dist-upgrade* to get the NVIDIA drivers correctly installed.


<a id="org2ce518f"></a>

## VNC Setup

Thanks to Nico for documentation!

Note that `$` indicates a command to be typed into a terminal.

-   Server (On Remote SUT)
    -   `$ sudo apt install tigervnc-standalone-server`
    -   `$ echo "kwin_x11 &" >> .vnc/xstartup`
    -   `$ echo "konsole" >> .vnc/xstartup`
    -   Start Server: `$ tigervncserver -geometry 1920x1080`
-   Client (On End-User Device)
    -   Create SSH tunnel: `$ ssh -N -L 5900:localhost:5901 kde@<INSERT-IP-HERE>`
    -   In client, e.g. [KRDC](<https://apps.kde.org/krdc/>): connect to localhost:5900
    -   This will start a single Konsole window, which can be used to start other programs.


<a id="org9cc77a4"></a>

## GUDE Power Meter Measurements

Thanks to Cornelius for the documentation!

From: <https://invent.kde.org/teams/eco/feep/-/blob/master/measurement_setup.md>

Note that `$` indicates a command to be typed into a terminal.

-   For [GUDE Expert Power Control 1202](<https://www.gude.info/en/power-distribution/switched-metered-pdu/expert-power-control-1202-series.html>) ([manual](<https://gude-systems.com/app/uploads/2022/05/manual-epc1202-series.pdf>))
    -   The device is controlled and read via cabled Ethernet.
    -   There is a web-based user interface, a [REST API](<http://wiki.gude.info/EPC_HTTP_Interface>), and the device supports various protocols such as SNMP or syslog.
    -   There is a Python script available to read data from the device via SNMP: <https://gitlab.rlp.net/green-software-engineering/mobiles-messgerat>

Notes how to make the Python script work (quick and dirty):

-   The script uses `snmpget` to read out measurement data via SNMP. On Ubuntu it can be installed with `$ sudo apt install snmp`
-   To install the necessary SNMP info you also need to install the package on Ubuntu: `$ sudo apt install snmp-mibs-downloader`
-   In the web interface of the measuring device you need to enable SNMP.
    -   Go to the Configuration tab, choose "Protocols" and then "SNMP" and check the checkboxes "Enable SNMP options". This enables SNMP v1 which is used by the measurement script.
-   You also need to download the SNMP information for the measurement device.
    -   Click the link "MIB table" to show it and then save it to your local machine as `GUDEADS-EPC1202-MIB.txt`.
    -   Store it on `/usr/share/snmp/mibs` and set the environment variable to activate it: `$ export MIBS=+GUDEADS-EPC1202-MIB`

To configure the script `get_energy_utilization.py`, which reads the measurement data and writes it to a file, you have to set some more enviornment variables:

-   `LOGFILE`: Set it to the name of the file where measurement data is stored.
-   `IP`: Set it to the host name or IP address of the measurement device.
-   `AMPERE_ADDRESS`: Set it to the SNMP object id of the current measurement. You can take it from the MIB file.
    -   Take OID of `epc1202Current` and append `.1` to get the value. This will look something like: `$ export AMPERE_ADDRESS=1.3.6.1.4.1.28507.43.1.5.1.2.1.5.1`
-   `VOLTAGE_ADDRESS`: Set it to the SNMP object id of the voltage measurement. You can take it from the MIB file.
    -   Take OID of `epc1202Voltage` and, again, append `.1` as above.
-   `POWERFACTOR_ADDRESS`: Take the OID of `epc1202PowerFactor` from the MIB file and append `.1` as above.

There seems to be an incompatibility in the script.

-   Change the last lines of the `getSNMP` method to:

> fields=line.split(": ")  
> return int(fields[1].split()[0])

-   After this, run the script. It will print the measurments to standard output and write them to the log file.
-   Stop the script with CTRL-C when the measurement is done.
-   The results looks like this:

> Zeit;Wert 1-avg[W]  
> 2022-05-21 17:52:22.838772; 23.760564  
> 2022-05-21 17:52:24.306634; 23.656351  
> 2022-05-21 17:52:25.502201; 23.70107  
> 2022-05-21 17:52:26.774169; 23.70107  
> 2022-05-21 17:52:27.837169; 23.656351  
> 2022-05-21 17:52:28.871263; 23.625252  
> 2022-05-21 17:52:29.917944; 23.656351  
> 2022-05-21 17:52:31.364232; 23.70107  
> 2022-05-21 17:52:33.329414; 23.656351  
> 2022-05-21 17:52:34.915585; 23.656351  
> 2022-05-21 17:52:35.986060; 23.80548  
> 2022-05-21 17:52:37.022003; 23.729328  
> 2022-05-21 17:52:39.152300; 23.639952  
> 2022-05-21 17:52:40.740454; 23.656351  
> 2022-05-21 17:52:41.804313; 23.760564  
> 2022-05-21 17:52:43.067764; 23.7156


<a id="orgcb28c12"></a>

# DISCUSSION

This is a summary of the discussion at the beginning of Sprint while planning. This is not exhaustive and only intended as an overview.

-   How to best use both computers? 
    -   One computer for System Under Test (SUT) and one computer for Data Aggregator? Or two computers with two different OSes?
    -   Final decision: Two options for System Under Test with two different OSes (Neon, Kubuntu), own computer as Data Aggregator.
-   Which version of OS do we need to install for Blauer Engel (BE)? 
    -   Criteria recommend Ubuntu.
    -   Question: Would other OSes be accepted as long as setup is transparent.
    -   And what is the goal of the lab in the big picture? Only for BE certification? Or for developers to measure their software?
    -   Final decision: Use KDE Neon and Kubuntu
-   Measuring different versions of same software with minimal changes to the system?
    -   How easy is it to build older versions of same software?
    -   Build Flatpak locally, send to remote system?
-   Problem of localizing changes in energy consumption for different versions of Flatpak, different kernels, etc.
    -   Importance of documenting all metadata to be able to find where issues may lie.
-   Focus on replicability of SUT or on representation of real-world? 
    -   Do we want a system that is maximally replicable, or do we want to identify regressions in software in development?
    -   Which is preferable? Stable reference system without updates? Real-world situation with updates?
-   What can be automated? What requires manual changes?
-   Importance of noting temperature of the lab!
    -   Problem of fans, etc. running in hot rooms.
    -   To compare different systems, need to measure as closely as possible to each other.

