# Minutes for the community meetup on 2023-11-08

* Start: 18:00 UTC; End: 19:00 UTC

* 8 participants [names removed to protect participant privacy]

* *Where*: BigBlueButton

* *Topic*: Continued discussion for sustainable software goal (see pad notes below).

  For general information about the Sustainable Software goal, see:

  https://community.kde.org/Goals/Sustainable_Software
  
  Minutes of the meetup are at the end of this document.

## Sustainable Software Goal Information

### Community Pad Notes

#### To Discuss

Please add your ideas!

* Cross-over goal presenter: Harald for Selenium testing
* Eco tab: Discuss upstream data from apps.kde.org for use in Autonomy tab (see https://invent.kde.org/teams/eco/sustainable-software-goal/-/issues/2#note_781261)

#### Progress updates

* Eco tab links
  + Template idea: https://invent.kde.org/teams/eco/sustainable-software-goal/-/issues/2
  + Draft: kaboutdata: add properties for eco information: https://invent.kde.org/frameworks/kcoreaddons/-/merge_requests/374
  + Draft: aboutapplicationdialog: add eco tab: https://invent.kde.org/frameworks/kxmlgui/-/merge_requests/187
* KEcoLab, to test: https://invent.kde.org/teams/eco/remote-eco-lab
  + Tutorial video: https://invent.kde.org/drquark/remote-eco-lab/-/blob/master/KEcolab-Tutorial.mp4
* KEcoLab, ready to test: https://invent.kde.org/teams/eco/remote-eco-lab
   + Tutorial video: https://invent.kde.org/drquark/remote-eco-lab/-/blob/master/KEcolab-Tutorial.mp4
* Integrating KEcoLab into development pipeline, starting with Okular
* Progress with Selenium/KdeEcoTest for energy consumption measurements
* Akademy BoF notes: https://invent.kde.org/teams/eco/be4foss/-/blob/master/community-meetups/2023-07-17_energy-measurements_bof.md
* Funding proposal "Sustainable Software For Sustainable Hardware" 
   + Collecting minimum system requirements, sustainability characteristics (offline use, freedom from ads) -- overlaps with Eco Tab template
   + Collecting info from community about unsupported hardware that runs Plasma well

#### To-dos and previous ideas

* Initiative: KDE Eco badge (internally-defined criteria)
* Initiative: KDE Sustainable Software Dashboard / Progress dashboard (to track what has been measured, certified, etc., with informative visualizations)
* Initiative: Eco widget for Plasma (provides users: eco, power grid mix, etc. info)
* Standard frameworks have an example (a UI design mockup would be a good start)
* Campaign: upcycling with Plasma?
* KUserFeedback: mean age of hardware running software, oldest hardware, etc.
* OpenGL version, parameters of screen setup (size, number of screens)
* Add age of mainboard, CPU, etc.?
* Metrics: User engagement with the project
* Outreach to possible natural allies (e.g., GreenPeace, WWF)
* Initiative: Measurement labs (see handbook above)
* Standards and tools for measurements (making measurements comparable)
* Any other ideas you have?

#### Previous Community Pad Notes

Included in minutes from past KDE Eco meetups at: <https://invent.kde.org/teams/eco/be4foss/-/tree/master/community-meetups>

#### Work Board

* GitLab Board: <https://invent.kde.org/teams/eco/sustainable-software-goal/-/boards>

  Feel free to create issues with ideas or things you want to work on.

  Feel also free to work on issues you are interested in. Just assign yourself to the card and comment there what you are doing.

#### Other Material

* KDE goals page: <<https://kde.org/goals/>
* Wiki page for Sustainable Software goal: <https://community.kde.org/Goals/Sustainable_Software>
* Presentation at Goal Kick-Off: <https://invent.kde.org/teams/eco/sustainable-software-goal/-/blob/master/presentations/sustainable-software-goal-kickoff.pdf>
* Original goal description: <https://phabricator.kde.org/T15676>

## Minutes

### C(omments)/Q(uestions)/A(nswers)

* Introductions

* Selenium
  + Use in other 2 KDE Goals, namely Accessibility and Automation 
  + Any improvements in one benefits the others, and there are many improvements already
  + Automation goal: Selenium can automate UI interaction, 20-30 tests already written
  + Currently Plasma workspace tests are available: <https://invent.kde.org/plasma/plasma-workspace/-/tree/master/appiumtests>
  + Running these tests also for energy consumption measurements (including CPU, memory, etc.), can provide early warning signs of regressions.
  + Everyone should try writing a test. Or when a test is failing in CI then try to fix it, it does not require much knowledge, it is just instructions to click buttons.
  + Selenium is also useful for testing a new feature, which cannot be done in a unit test. See Tokodon!
  + Q: Can the existing tests already be run in KEcoLab? A: Yes, integrating this with the remote lab should be feasible
  + Q: The current guide focuses on GCompris, are there other examples? A: Yes, see <https://invent.kde.org/sdk/selenium-webdriver-at-spi/-/wikis/writing-tests>. One can write a test for everything that has accessibility support, which is 95% of app market on Linux. It is very generic, you can represent any interface in the API.
  + Q: How can we communicate with each other so we are up-to-date on what is happening with tests for the other goals? A: When MRs are made, a notification can be automated.
  + Q: Who should we contact if we have questions? A: Write to any development room if there are questions, also the automation goals room.

* Eco/Autonomy tab
  + Addressing duplication data, i.e., both upstream for apps.kde.org and the proposed Autonomy tab.
  + Upstream has a custom field where we can add anything we want.
  + Source of information is inside the repository of the app itself. Each app can provide specific info.
  + Q: How would this work exactly? A: No definitive answer yet, probably a larger re-working of "About" data handling, we can probably load the information from upstream with custom fields.
  + Q: Should we wait for this to be implemented, or can we do something now even if more basic?
  + Q: For upstream: where do we put links? A: In upstream data, can put any information we want in the custom tab. That way it can be available in all the websites as well. <https://freedesktop.org/software/appstream/docs/chap-Metadata.html#tag-custom>
  + C: We are missing how that info goes through KAboutData.
  + C: We do not have a link to source code right now, that can be added.
  + C: Separate the two tasks: do something specific for the Eco tab and eventually we can expand that to KAboutData.
  + C: That is related to what we discussed last time. We do not need the full frameworks level API yet, ECO tab is so specific. We can do appstream backing later. We dont need public Frameworks for this.
  + C: So for now what we want is the info to be fed from upstream. But that does not exist yet. The file exists, but the Eco info does not exist.
  + C: The plan to link to the Okular website is still relevant. We need that to be done.
  + Q: Where should we store images? A: In the application with custom text until more apps are certified.
  + Design needs to be discussed with VDG. Simple design, put all of the detailed information on the website. It needs to look pretty, and then it is useable.
  + Four to-dos:
    1. Upstream implementation: need link by defining a custom tag in the application repository, see <https://invent.kde.org/graphics/okular/-/blob/master/shell/org.kde.okular.appdata.xml>
    2. Eco/Autonomy tab implementation
    3. Website page with info, e.g., okular.kde.org/eco or at eco.kde.org/okular
    4. Generation script for apps.kde.org